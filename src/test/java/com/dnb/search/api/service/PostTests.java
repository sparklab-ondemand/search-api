package com.dnb.search.api.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.dao.AbstractDao;
import com.dnb.search.api.dao.IdDaoGetImpl;
import com.dnb.search.api.dao.IdsDaoPostImpl;
import com.dnb.search.api.entity.AppIdHeader;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.PostCountBodyResponse;
import com.dnb.search.api.entity.PostCountBodyResponseBody;
import com.dnb.search.api.entity.PostIdsBodyResponse;
import com.dnb.search.api.entity.ReturnBody;
import com.dnb.search.api.service.count.CountPost;
import com.dnb.search.api.service.record.RetrieveGet;
import com.dnb.search.api.service.record.RetrievePost;


public class PostTests {
	
	HttpServletRequest mockReq;
	HttpServletResponse mockResp;
	String postQuery;
	String apiid;
	AbstractDao testDao;
	ReturnBody retBody;
	static int actualTotal1;
	static int actualTotal2;
	
	@Before
	public void initialize(){
		this.mockReq=Mockito.mock(HttpServletRequest.class);
		this.mockResp=Mockito.mock(HttpServletResponse.class);
		
		JSONObject postQueryJSON=new JSONObject();
		JSONObject queryJSON=new JSONObject();
		queryJSON.put("match_all", new JSONObject());
		postQueryJSON.put("query", queryJSON);
		this.postQuery=postQueryJSON.toString();
		
		this.apiid=Constants.DEFAULT_API_ID_1;
		Mockito.when(mockReq.getRequestURI()).thenReturn("mock/api/v1/mock-test/datasets/");	
		
		SearchInitializer init=new SearchInitializer();
		init.reload();
	}

	
	@Test
	public void testPostCount(){

		SearchService searchService=new SearchService();
		Response response=searchService.countPOST(this.mockReq, this.mockResp, this.apiid, this.postQuery);
		
		String retVal=(String)response.getEntity();
		JSONObject retValJSON=new JSONObject(retVal);
		assertTrue(retValJSON.has(Constants.TOTAL_STR));
		
		PostTests.actualTotal1=retValJSON.getInt(Constants.TOTAL_STR);
		assertTrue(PostTests.actualTotal1>0);	
		
		assertTrue(retValJSON.has(Constants.URL_STR));
		String url = retValJSON.getString(Constants.URL_STR);
		assertTrue(url!=null);
		
		assertTrue(retValJSON.has(Constants.HEADER_STR));
		
		JSONObject headerJSON=retValJSON.getJSONObject(Constants.HEADER_STR);
		
		assertTrue(headerJSON.has(Constants.X_API_ID_HEADER));
		String header=headerJSON.getString(Constants.X_API_ID_HEADER);
		
		assertTrue(header!=null);
		
		assertTrue(retValJSON.has(Constants.BODY_STR));
		JSONObject bodyJSON=retValJSON.getJSONObject(Constants.BODY_STR);
				
		assertTrue(bodyJSON.has(Constants.QUERY_STR));	
		
	}
	
	@Test
	public void testPostID(){
		SearchService searchService=new SearchService();
		Response response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, "5m", null, 2, "dnb-default1", this.postQuery);
		
		PostIdsBodyResponse postIds=(PostIdsBodyResponse)response.getEntity();
		PostTests.actualTotal2=postIds.getTotal();
		
		String scrollId=postIds.getScrollid();
		//We should be given a scrollid
		assertTrue(scrollId!=null);

		//Now call the fetch Ids with the scrollid
		response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, null, scrollId, 0, "dnb-default1", null);
		postIds=(PostIdsBodyResponse)response.getEntity();
		
		//We should have ids
		List<String>ids=postIds.getIds();
		assertTrue(ids.size()>0);

		
	}
	
	@Test
	public void testSameQueryCount(){
		//since both queries are the same the totals should be the same
		assertTrue(GetTests.actualTotal1==GetTests.actualTotal2);
	}
	
	@Test
	public void testPostIDNoAPIID(){
		SearchService searchService=new SearchService();
		
		//Make the POST ID call WITHOUT APPID
		Response response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, "5m", null, 2, null, this.postQuery);
		Error errorResponse=(Error)response.getEntity();
		//should generate an error
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testBadJsonResponse(){
		//simulates the server returning a bad json in its response : causes an internal server error
		this.testDao=new IdsDaoPostImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdsDaoPostImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("abc");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();

		Response response=testPost.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", null, null, null, null, 2);
		assertTrue(response.getStatus()==500);
	}
	
	@Test
	public void testJsonResponseError(){
		this.testDao=new IdsDaoPostImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdsDaoPostImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"error\":{\"root_cause\": [{\"error1\":\"err_cause1\"},{\"error2\":\"err_cause2\"}]}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();
		
		testPost.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", null, null, null, null, 2);//(this.mockReq, this.mockResp, testGet, "dnb-default1", "", "", "","", 1);
		//'{"error": "Hello World"}'
	}
	
//	@Test
//	public void testPostIDWrongAPIID(){
//		SearchService searchService=new SearchService();
//		
//		//Make the POST ID call WITHOUT APPID
//		Response response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, "5m", null, 2, "dnb-xyz", this.postQuery);
//		Error errorResponse=(Error)response.getEntity();
//		//should generate an error
//		assertTrue(errorResponse.hasErrors());
//	}
	
	@Test
	public void testPostCountNoAPIID(){
		SearchService searchService=new SearchService();
		//Making the POST Count call WITHOUT APIID
		Response response=searchService.countPOST(this.mockReq, this.mockResp, null, this.postQuery);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testPostCountWrongAPIID(){
		SearchService searchService=new SearchService();
		//Making the POST Count call WITHOUT APIID
		Response response=searchService.countPOST(this.mockReq, this.mockResp, "dnb-xyz", this.postQuery);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testPostIDOne(){
		SearchService searchService=new SearchService();
		Response response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, null, null, 2, "dnb-default1", this.postQuery);
		
		PostIdsBodyResponse postIds=(PostIdsBodyResponse)response.getEntity();
		PostTests.actualTotal2=postIds.getTotal();
		
		String scrollId=postIds.getScrollid();
		//We should be given a scrollid
		assertTrue(scrollId!=null);

		//Now call the fetch Ids with the scrollid and scroll
		response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, "2m", scrollId, 0, "dnb-default1", null);
		postIds=(PostIdsBodyResponse)response.getEntity();
		
		//We should have ids
		List<String>ids=postIds.getIds();
		assertTrue(ids.size()>0);
	}
	
	@Test
	public void testPostCountNoQuery(){
		SearchService searchService=new SearchService();
		//Making the POST Count call WITHOUT APIID
		Response response=searchService.countPOST(this.mockReq, this.mockResp, "dnb-default1", null);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testPostCountBadQuery(){
		SearchService searchService=new SearchService();
		//Making the POST Count call WITHOUT APIID
		Response response=searchService.countPOST(this.mockReq, this.mockResp, "dnb-default1", "abc");
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testPostIDBadQuery(){
		SearchService searchService=new SearchService();
		
		JSONObject postQueryJSON=new JSONObject();
		JSONObject queryJSON=new JSONObject();
		queryJSON.put("match_al", new JSONObject());
		postQueryJSON.put("query", queryJSON);
		this.postQuery=postQueryJSON.toString();
		
		//Make the POST ID call WITHOUT APPID
		//{"query":{"ter":{"siteCity":"austin"}}}
		Response response=searchService.countPOST(this.mockReq, this.mockResp, "dnb-default1", this.postQuery);
		Error errorResponse=(Error)response.getEntity();
		//should generate an error
		assertTrue(errorResponse.hasErrors());
	}
	
	@Test
	public void testPostIDNoQueryNoScrollId(){
		SearchService searchService=new SearchService();
		//Making the POST Count call WITHOUT APIID
		Response response=searchService.fetchRecordsPOST(this.mockReq, this.mockResp, "5m", null, 2, null, null);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}
	
	
	@Test
	public void testNoHitsInReponse(){
		//Response Body is Missing Hits
		IdsDaoPostImpl testDao=new IdsDaoPostImpl();
		ReturnBody retBody=Mockito.mock(ReturnBody.class);
		testDao=Mockito.mock(IdsDaoPostImpl.class);
		Mockito.when(retBody.getResults()).thenReturn("{\"counta\":969265, \"results\":{\"total\":100}, \"timed_out\":false, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(retBody.getStatusCode()).thenReturn(200);

		Mockito.when(testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();

		Response response = testPost.handlePassthruService(this.mockReq, this.mockResp, testDao, "dnb-default1", this.postQuery, this.postQuery, "2m", "abcefg123", 2);
		assertTrue(response.getStatus()==500);
	}
	
	@Test
	public void testJsonNoSecondHits() throws JSONException{
		//Simulates the server replying with a bad response, with no Hits
		this.testDao=new IdsDaoPostImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":100}, \"timed_out\":false, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();

		Response response = testPost.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.postQuery, null, null, null, 2);
		assertTrue(response.getStatus()==204);
	}
	
	@Test
	public void testScrollIdStale() throws JSONException{
		//Simulates a call with scrollID thats timed out
		this.testDao=new IdsDaoPostImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":100}, \"timed_out\":true, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();

		Response response = testPost.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.postQuery, null, null, "abcdefghi123", 2);
		assertTrue(response.getStatus()==500);
	}
	
	@Test
	public void testTotalZero() throws JSONException{
		//Simulates a call with scrollID thats timed out
		this.testDao=new IdsDaoPostImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdsDaoPostImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":0}, \"timed_out\":true, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrievePost testPost=new RetrievePost();

		Response response = testPost.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.postQuery, null, null, "abcdefghi123", 2);
		PostIdsBodyResponse postResponse = (PostIdsBodyResponse)response.getEntity();
		assertTrue(postResponse.getCount()==0);

		//		GetCountResponse getResponse=(GetCountResponse)response.getEntity();
		//		AppIdHeader appIdHeader=getResponse.getHeader();
		//		String id=appIdHeader.getAppid();
		//		GetTests.actualTotal1= getResponse.getTotal();
		//		assertTrue(GetTests.actualTotal1>0);
		//		assertTrue(id.equals("dnb-default1"));
	}
	
	public static void main(String[]args) throws JSONException{
		PostTests count=new PostTests();
		
		count.initialize();
		count.testPostCount();
//		count.testTotalZero();
//		count.testScrollIdStale();
//		count.testJsonNoSecondHits();
//		count.testNoHitsInReponse();
//		count.testBadJsonResponse();
//		count.testJsonResponseError();
//		count.testPostCount();
//		count.testPostID();
//		count.testSameQueryCount(); //perform only after testPostCount + testPostID
//		count.testPostCountNoAPIID();
//		count.testPostCountWrongAPIID();
//		count.testPostCountNoQuery();
//		count.testPostCountBadQuery();
//		count.testPostIDNoAPIID();
//		count.testPostIDOne();
//		count.testPostIDBadQuery();
//		count.testPostIDNoQueryNoScrollId();
		
	}
}
