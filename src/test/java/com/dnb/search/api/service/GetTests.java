package com.dnb.search.api.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.dao.AbstractDao;
import com.dnb.search.api.dao.IdDaoGetImpl;
import com.dnb.search.api.entity.AppIdHeader;
import com.dnb.search.api.entity.GetCountResponse;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.PostIdsBodyResponse;
import com.dnb.search.api.entity.ReturnBody;
import com.dnb.search.api.service.count.CountGet;
import com.dnb.search.api.service.record.RetrieveGet;

public class GetTests {
	HttpServletRequest mockReq;
	HttpServletResponse mockResp;
	String getQuery;
	String apiid;
	String bigQuery;
	static int actualTotal1;
	static int actualTotal2;
	AbstractDao testDao;
	ReturnBody retBody;

	@Before
	public void initialize(){
		this.mockReq=Mockito.mock(HttpServletRequest.class);
		this.mockResp=Mockito.mock(HttpServletResponse.class);
		this.getQuery="siteStateCode:tx";
		this.bigQuery="siteStateCode:tx AND siteCity:maria";
		this.apiid=Constants.DEFAULT_API_ID_1;
		Mockito.when(mockReq.getRequestURI()).thenReturn("mock/api/v1/mock-test/datasets/");	

		SearchInitializer init=new SearchInitializer();
		init.reload();
	}


	@Test
	public void testBadJsonResponse(){
		//simulates the server returning a bad json in its response : causes an internal server error
		this.testDao=new IdDaoGetImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("abc");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		Response response=testGet.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", null, null, null, null, 2);
		assertTrue(response.getStatus()==500);
	}

	@Test
	public void testTotalZero() throws JSONException{
		//Simulates a call with scrollID thats timed out
		this.testDao=new IdDaoGetImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":0}, \"timed_out\":true, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		Response response = testGet.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.getQuery, null, null, "abcdefghi123", 2);
		PostIdsBodyResponse getResponse = (PostIdsBodyResponse)response.getEntity();
		assertTrue(getResponse.getCount()==0);

		//		GetCountResponse getResponse=(GetCountResponse)response.getEntity();
		//		AppIdHeader appIdHeader=getResponse.getHeader();
		//		String id=appIdHeader.getAppid();
		//		GetTests.actualTotal1= getResponse.getTotal();
		//		assertTrue(GetTests.actualTotal1>0);
		//		assertTrue(id.equals("dnb-default1"));
	}

	@Test
	public void testScrollIdStale() throws JSONException{
		//Simulates a call with scrollID thats timed out
		this.testDao=new IdDaoGetImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":100}, \"timed_out\":true, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		Response response = testGet.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.getQuery, null, null, "abcdefghi123", 2);
		assertTrue(response.getStatus()==500);
	}

	@Test
	public void testJsonNoSecondHits() throws JSONException{
		//Simulates the server replying with a bad response, with no Hits
		this.testDao=new IdDaoGetImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"counta\":969265, \"hits\":{\"total\":100}, \"timed_out\":false, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		Response response = testGet.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", this.getQuery, null, null, null, 2);
		assertTrue(response.getStatus()==204);
	}

	@Test
	public void testJsonResponseError(){
		this.testDao=new IdDaoGetImpl();
		this.retBody=Mockito.mock(ReturnBody.class);
		this.testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(this.retBody.getResults()).thenReturn("{\"error\":{\"root_cause\": [{\"error1\":\"err_cause1\"},{\"error2\":\"err_cause2\"}]}}");
		Mockito.when(this.retBody.getStatusCode()).thenReturn(200);

		Mockito.when(this.testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		testGet.handlePassthruService(this.mockReq, this.mockResp, this.testDao, "dnb-default1", null, null, null, null, 2);//(this.mockReq, this.mockResp, testGet, "dnb-default1", "", "", "","", 1);
		//'{"error": "Hello World"}'
	}

	@Test
	public void testGetCount(){
		SearchService searchService=new SearchService();

		//make GET Count call with header & query
		Response response=searchService.countGET(this.mockReq, this.mockResp, "dnb-default1", this.getQuery);

		GetCountResponse getResponse=(GetCountResponse)response.getEntity();
		AppIdHeader appIdHeader=getResponse.getHeader();
		String id=appIdHeader.getAppid();
		GetTests.actualTotal1= getResponse.getTotal();
		assertTrue(GetTests.actualTotal1>0);
		assertTrue(id.equals("dnb-default1"));
	}

	@Test
	public void testGetIDOne(){
		//		CountGet countGet=new CountGet();
		SearchService searchService=new SearchService();

		//make GET ID call with header & query
		Response response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, "2m", null, 5, "dnb-default1", this.getQuery);

		//the GetID method returns a PostIdsBodyResponse
		PostIdsBodyResponse getIDResponse = (PostIdsBodyResponse)response.getEntity();


		GetTests.actualTotal2=getIDResponse.getTotal();
		assertTrue(GetTests.actualTotal2>0);

//		Response response2=searchService.fetchRecordsGET(this.mockReq, this.mockResp, "2m", getIDResponse.getScrollid(),
//				2, "dnb-default1", this.getQuery);
//		getIDResponse=(PostIdsBodyResponse) response2.getEntity();
//		List<String>ids = getIDResponse.getIds();
//		assertTrue(ids.size()>0);
	}

	@Test
	public void testSameQueryCount(){
		//since both queries are the same the totals should be the same
		assertTrue(GetTests.actualTotal1==GetTests.actualTotal2);
	}

	@Test
	public void testGetCountNoAPIID(){
		SearchService searchService=new SearchService();
		//make GET Count call WITHOUT Header
		Response response2=searchService.countGET(this.mockReq, this.mockResp, null, this.getQuery);
		Error errorResponse=(Error)response2.getEntity();
		assertTrue(errorResponse.hasErrors());
	}

	@Test
	public void testGetCountNoQueryNoAPIID(){
		SearchService searchService=new SearchService();
		//make GET Count call WITHOUT Header or QUERY
		Response response=searchService.countGET(this.mockReq, this.mockResp, null, null);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}

	@Test
	public void testGetCountBadAPIID(){
		SearchService searchService=new SearchService();
		//make GET Count call with incorrect APIID
		Response response=searchService.countGET(this.mockReq, this.mockResp, "dnb-xyz", this.getQuery);
		Error errorResponse=(Error)response.getEntity();
		assertTrue(errorResponse.hasErrors());
	}

	@Test
	public void testGetIDNOAPIID(){
		SearchService searchService=new SearchService();
		//make GET Count call WITHOUT APIID 
		Response response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, "5m", null, 2, null, this.getQuery);
		Error errorResponse=(Error)response.getEntity();
		//should generate an error
		assertTrue(errorResponse.hasErrors());
	}

	@Test
	public void testGetIDNoScrollNoQuery(){
		SearchService searchService=new SearchService();
		//make GET Count call WITHOUT APIID 
		Response response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, "5m", null, 2, null, null);
		Error errorResponse=(Error)response.getEntity();
		//should generate an error
		JSONObject jsonErrorObj = errorResponse.toJSON();
		assertTrue(jsonErrorObj!=null);
		assertTrue(errorResponse.hasErrors());
	}

	//@Test
	public void testGetIDWithScroll(){
		SearchService searchService=new SearchService();

		//make GET ID call with header & query
		Response response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, null, null, 5, "dnb-default1", this.bigQuery);

		//the GetID method returns a PostIdsBodyResponse
		PostIdsBodyResponse getIDs = (PostIdsBodyResponse)response.getEntity();

		String scrollid = getIDs.getScrollid();
		assertTrue(scrollid!=null);

		int totalCount = getIDs.getTotal();
		int counter = 0;

		while (counter<totalCount){
			response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, null, scrollid, 0, "dnb-default1", null);
			getIDs=(PostIdsBodyResponse) response.getEntity();

			List<String>ids = getIDs.getIds();

			assertTrue(ids.size()>0);
			scrollid=getIDs.getScrollid();
			counter+=getIDs.getCount();
		}
		assertTrue(counter==totalCount);
		response=searchService.fetchRecordsGET(this.mockReq, this.mockResp, null, scrollid, 0, "dnb-default1", null);
		getIDs=(PostIdsBodyResponse) response.getEntity();

		//		List<String>ids = getIDs.getIds();

		//		assertTrue(ids.size()>0);
		//		scrollid=getIDs.getScrollid();
		//		counter+=getIDs.getCount();



		//		GetTests.actualTotal2=getIDResponse.getTotal();
		//		assertTrue(GetTests.actualTotal2>0);

	}

	@Test
	public void testNoHitsInReponse(){
		//Response Body is Missing Hits
		IdDaoGetImpl testDao=new IdDaoGetImpl();
		ReturnBody retBody=Mockito.mock(ReturnBody.class);
		testDao=Mockito.mock(IdDaoGetImpl.class);
		Mockito.when(retBody.getResults()).thenReturn("{\"counta\":969265, \"results\":{\"total\":100}, \"timed_out\":false, \"_shards\":{\"abc\":\"xyz\"}}");
		Mockito.when(retBody.getStatusCode()).thenReturn(200);

		Mockito.when(testDao.processSearch()).thenReturn(retBody);
		RetrieveGet testGet=new RetrieveGet();

		Response response = testGet.handlePassthruService(this.mockReq, this.mockResp, testDao, "dnb-default1", this.getQuery, this.getQuery, "2m", "abcefg123", 2);
		assertTrue(response.getStatus()==500);
	}

	public static void main(String[] args) throws JSONException{
		GetTests getTests=new GetTests();
		getTests.initialize();
//		getTests.testJsonNoSecondHits();
//		getTests.testScrollIdStale();

//		getTests.testBadJsonResponse();
//		getTests.testJsonResponseError();
		getTests.initialize();
		getTests.testGetCount();
//		getTests.testNoHitsInReponse();		
//		getTests.testGetIDOne();
//
//		getTests.testSameQueryCount();
//		getTests.testGetCountNoAPIID();
//		getTests.testGetCountNoQueryNoAPIID();
//		getTests.testGetCountBadAPIID();
//		getTests.testGetIDNoScrollNoQuery();
//		getTests.testGetIDWithScroll();

	}
}
