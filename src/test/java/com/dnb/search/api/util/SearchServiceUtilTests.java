package com.dnb.search.api.util;

import static org.junit.Assert.assertTrue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.PostIdsBodyResponse;
import com.dnb.search.api.entity.WhereClause;
import com.dnb.search.api.service.SearchService;

public class SearchServiceUtilTests {
	
	HttpServletRequest mockReq;
	HttpServletResponse mockResp;
	String getQuery;
	String badQuery;
	String apiid;
	static int actualTotal1;
	static int actualTotal2;
	
	@Before
	public void initialize(){
		this.mockReq=Mockito.mock(HttpServletRequest.class);
		this.mockResp=Mockito.mock(HttpServletResponse.class);
		this.getQuery="siteCity='Austin' AND siteStateCode='TX'";
		this.badQuery="siteCity='Austin' AN'D state='texas'";
		this.apiid=Constants.DEFAULT_API_ID_1;
		Mockito.when(mockReq.getRequestURI()).thenReturn("mock/api/v1/mock-test/datasets/");	
		
		SearchInitializer init=new SearchInitializer();
		init.reload();
	}
	
	@Test
	public void testGetCount(){
		SearchService searchService = new SearchService();
		Response response=searchService.sqlSearchGet(this.mockReq, this.mockResp, "dnb-default1", this.getQuery);
		
//		Error errorResponse=(Error)response.getEntity();
//		//should generate an error
//		assertTrue(errorResponse.hasErrors());

		String retVal=(String)response.getEntity();
		JSONObject retValJSON=new JSONObject(retVal);
		assertTrue(retValJSON.has(Constants.TOTAL_STR));
		SearchServiceUtilTests.actualTotal1=retValJSON.getInt(Constants.TOTAL_STR);

		assertTrue(SearchServiceUtilTests.actualTotal1>0);	
	}
	
//	@Test
//	public void testGetCountBadQuery(){
//		SearchService searchService = new SearchService();
//		Response response=searchService.sqlSearchGet(this.mockReq, this.mockResp, "dnb-default1", "abc%2%72");
//		
//		Error errorResponse=(Error)response.getEntity();
//		//should generate an error
//		assertTrue(errorResponse.hasErrors());
//	}
	
	@Test
	public void testPostCount(){
		WhereClause clause = new WhereClause();
		clause.setClause(this.getQuery);
		SearchService searchService = new SearchService();
		
		Response response=searchService.sqlSearchPost(this.mockReq, this.mockResp, "dnb-default1", clause);

		String retVal=(String)response.getEntity();
		JSONObject retValJSON=new JSONObject(retVal);
		assertTrue(retValJSON.has(Constants.TOTAL_STR));
		SearchServiceUtilTests.actualTotal1=retValJSON.getInt(Constants.TOTAL_STR);
		assertTrue(SearchServiceUtilTests.actualTotal1>0);
	}
	
	@Test
	public void testPostIds(){
		WhereClause clause = new WhereClause();
		clause.setClause(this.getQuery);
		SearchService searchService = new SearchService();
		Response response=searchService.sqlIdsPost(this.mockReq, this.mockResp, "2m", null, 2, "dnb-default1", clause);

		PostIdsBodyResponse retVal=(PostIdsBodyResponse)response.getEntity();
		SearchServiceUtilTests.actualTotal1=retVal.getTotal();
		assertTrue(SearchServiceUtilTests.actualTotal1>0);
	}
	
	@Test
	public void testGetIds(){
//		WhereClause clause = new WhereClause();
//		clause.setClause(this.getQuery);
		SearchService searchService = new SearchService();
		Response response=searchService.sqlIdsGet(this.mockReq, this.mockResp, "2m", null, 2, "dnb-default1", this.getQuery);

		PostIdsBodyResponse retVal=(PostIdsBodyResponse)response.getEntity();
		SearchServiceUtilTests.actualTotal1=retVal.getTotal();
		assertTrue(SearchServiceUtilTests.actualTotal1>0);
	}
	
	@Test
	public void testPostIdsBadQuery(){
//		WhereClause clause = new WhereClause();
//		clause.setClause(this.getQuery);
		Response response=SearchServiceUtil.handleSQLGetIds(this.mockReq, this.mockResp, this.badQuery, "dnb-default1", "2m", null, 2);

		Error errorResponse=(Error)response.getEntity();
		//should generate an error
		assertTrue(errorResponse.hasErrors());
	}
	
	public static void main(String[] args){
		SearchServiceUtilTests searchServiceTests = new SearchServiceUtilTests();
		searchServiceTests.initialize();
//		searchServiceTests.testPostCount();
//		searchServiceTests.testGetCount();
//		searchServiceTests.testGetCountBadQuery();
//		searchServiceTests.testPostCount();
//		searchServiceTests.testGetIds();
//		searchServiceTests.testPostIds();
	}
}
