package com.dnb.search.api.entity;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class WhereClauseTest {
	
	@Test
	public void toStringTest(){
		WhereClause clause = new WhereClause();
		clause.setClause("siteCity='Austin' OR siteCity='Boston'");
		assertTrue(clause.toString()!=null);
		clause.setClause("siteCity='Austin' OR siteCity='Boston");
		
	}
	
	public static void main(String[] args){
		WhereClauseTest clauseTest = new WhereClauseTest();
		
	}

}
