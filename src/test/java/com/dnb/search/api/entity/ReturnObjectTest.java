package com.dnb.search.api.entity;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.AssertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;

public class ReturnObjectTest {
	
	@Before
	public void initialize(){
		
		SearchInitializer init=new SearchInitializer();
		init.reload();
	}
	
	@Test
	public void testGetSet(){
		ReturnBody retBod = new ReturnBody();
		Date date = new Date();
		retBod.setStartProcessTime(date.getTime());
		retBod.setEndProcessTime(date.getTime());
		assertTrue(retBod.getStartProcessTime()!=null);
		assertTrue(retBod.getEndProcessTime()!=null);
		assertTrue(retBod.getApiProcessTime()!=null);
	}
	
	@Test
	public void testProcTime(){
		ReturnBody retBod = new ReturnBody();
		Date date = new Date();
		retBod.setStartProcessTime(date.getTime());
		
		assertTrue(retBod.getStartProcessTime()!=null);
		assertTrue(retBod.getApiProcessTime()!=null);
	}
	
	
	public static void main(String[] args){
		ReturnObjectTest retObjTest = new ReturnObjectTest();
		retObjTest.testGetSet();
		retObjTest.testProcTime();
	}
}
