package com.dnb.search.api.entity;

import static org.junit.Assert.assertTrue;

import javax.validation.constraints.AssertTrue;

import org.junit.Test;

public class PostCountBodyResponseBodyTest {
	
	@Test
	public void testGetSet(){
		PostCountBodyResponseBody resBod = new PostCountBodyResponseBody();
		resBod.setSize(5);
		resBod.setQuery("abc=texas");
		assertTrue(resBod.getSize()>0);
		assertTrue(resBod.getQuery()!=null);
	}
	
	public static void main(String[] args){
		PostCountBodyResponseBodyTest resBodTest = new PostCountBodyResponseBodyTest();
		resBodTest.testGetSet();
	}
}
