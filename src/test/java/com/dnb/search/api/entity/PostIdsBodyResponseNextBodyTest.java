package com.dnb.search.api.entity;

import static org.junit.Assert.assertTrue;

import javax.validation.constraints.AssertTrue;

import org.junit.Test;

public class PostIdsBodyResponseNextBodyTest {
	
	@Test
	public void testGetSet(){
		PostIdsBodyResponseNextBody resBod = new PostIdsBodyResponseNextBody();
		resBod.setScroll("5m");
		resBod.setScrollid("abcdefgh");
		assertTrue(resBod.getScroll()!=null);
		assertTrue(resBod.getScrollid()!=null);
	}
	
	public static void main(String[] args){
		PostIdsBodyResponseNextBodyTest resBodTest = new PostIdsBodyResponseNextBodyTest();
		resBodTest.testGetSet();
	}
}
