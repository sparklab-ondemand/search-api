package com.dnb.search.api;

public final class Constants {
	public static final String SEARCH_API_PROPS_FILE="search.properties";
	public static final String SEARCH_URL_COMMAND="_search";
	public static final String API_IDS_KEY="apiids";
	public static final String API_ID_HEADER_NAME="X-APP-ID";
	public static final String API_ID_DELIMETER=";";
	public static final String DEFAULT_API_ID_1="dnb-default1";
	public static final String DEFAULT_API_ID_2="dnb-default2";
	public static final String DEFAULT_API_IDS=DEFAULT_API_ID_1+";"+DEFAULT_API_ID_2;
	
	public static final String ERRORS_STR="errors";
	public static final String JSON_PARSE_ERROR="JSON Parsing Error";
	public static final String IO_EXEPTION_ERROR_STR="IOException Error";
	public static final String UNSUPPORTED_ENCODING_EXEPTION_ERROR_STR="UnsupportedEncodingException Error";
	
	public static final String SEARCH_API_DOES_NOT_ACCCESS=" APP ID does not have access";
	
	public static final String CATALINA_SEARCH_INDEX_PROP_NAME="SEARCH_INDEX";
	public static final String CATAINA_SEARCH_HOST_PROP_NAME="SEARCH_HOST";
	
	public static final String SEARCH_INDEX_NAME="dnb_search";
	public static final String _COUNT_STR="_count";
	public static final String COUNT_STR="count";
	public static final String HEADER_STR="header";
	public static final String BODY_STR="body";
	public static final String URL_STR="url";
	public static final String HITS_STR="hits";
	public static final String TOTAL_STR="total";
	public static final String SEARCH_HOST_NAME="http://localhost:9200/";
	public static final String SIZE_ZERO="&size=0";
	public static final String SEARCH_URL="url"; 
	public static final String ADNVANCED_GET_URL="/search/advance/count/query/";
	public static final String ADNVANCED_GET_ID_URL="/search/advanced/ids/query/";
	public static final String ADVANCED_GET_ID_WITH_SCROLL="/search/advanced/ids?scrollid=";
	public static final String SCROLL_URL_STR="&scroll=";
	public static final String SIZE_EQUALS="&size=";
	public static final String QUOTES = "\"";
	
	public static final String X_QUERY_HEADER="X-QUERY-HEADER";
	public static final int    X_QUERY_MAX_LENGTH=1000;
	public static final String X_API_ID_HEADER="X-APP-ID";
	public static final String X_ES_TOOK_HEADER="X-ES-TOOK";
	public static final String X_API_TOOK_HEADER="X-API-TOOK";
	
	public static final String QUERY_STR="query";
	public static final String QUERY_STR_PARAM="q";
	
	public static final String SEARCH_PARAMS_REQUIRED="apiid and query parameters are required.";
	public static final String API_HEADER_REQUIRED="Missing X-APP-ID header value";
	public static final String QUERY_REQUIRED="query parameter requires a value.";
	public static final String QUERY_CLAUSE_REQUIRED="query clause requires a value.";
	public static final String QUERY_CLAUSE_OR_SROLL_ID_REQUIRED="The query clause and scrollid cannot not both be empty.";
	public static final String SIZE_STR="size";
	
	public static final String ERROR_STR="error";
	public static final String TOOK_STR="took";
	public static final String END_POINT_LHS_DELIMETER="webapi";
	public static final String FETCH_IDS_URL_SUFFIX="ids";
	public static final String DEFAULT_SCROLL_SUFFIX="scroll=1m";
	
	public static final String STATUS_STR="status";
	public static final String ROOT_CLAUSE_STR="root_cause";
	
	public static final String RETRIEVE_IDS_MUST_HAVE_QUERY_OR_SCROLL_ID="Retrieval of DUNS ids must contain a scrollid or query DSL value";
	
	public static final int    DEFAULT_PAGE_SIZE=100;
	public static final String DEFAULT_SCROLL_VALUE="1m";
	public static final String SCAN_SEARCH_TYPE_SUFFIX="search_type=scan";
	public static final String SCROLL_STR="scroll";
	public static final String NO_JSONARRAY_ERROR="Cannot find the hits JSONArray";
	public static final String _SOURCE_STR="_source";
	public static final String _SCROLL_ID_STR="_scroll_id";
	public static final String SCROLL_ID_STR="scroll_id";

	public static final String DUNS_CAP_STR="DUNS";

	public static final String DUNS_STR="siteDuns";

	public static final String SCROLL_SCAN_SEARCH_URL_SUFFIX="_search/scroll";
	public static final String NO_HITS_FOUND="Unknown error, cannot not retrieve Ids";
	public static final String _SHARDS_STR="_shards";
	public static final String FAILED_STR="failed";
	public static final String FAILURES_STR="failures";
	public static final String TIMED_OUT_STR="timed_out";
	public static final String LHS_SCROLL_ID_NO_LONGER_VALID="The scrollid: ";
	public static final String RHS_SCROLL_ID_NO_LONGER_VALID=" is no longer valid, remove the scrollid from the request and retrieve another scrollid and retry";
	public static final String UNKOWN_ERROR="Unknown ERROR";
	
	public static final String SQL_COUNT_GET="sql/count/clause/";
	public static final String SQL_COUNT_POST="sql/count";
	public static final String SQL_IDS_POST="sql/ids";
	public static final String SQL_IDS_GET="sql/ids/clause/";
	public static final String EXPLAIN_URL_SUFFIX="_sql/_explain";
	public static final String SELECT_STAR_STR="SELECT * FROM ";
	public static final String UTF_STR="UTF-8";
	
	public static final String TYPE_STR="type";
	public static final String REASON_STR="reason";
	public static final String DUNS_SOURCE_HEADER_NAME="D-U-N-S";
}

