package com.dnb.search.api.service.record;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.dao.AbstractDao;
import com.dnb.search.api.dao.DaoFactory;
import com.dnb.search.api.dao.IdDaoGetImpl;
import com.dnb.search.api.entity.AppIdHeader;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.PostIdsBodyResponse;
import com.dnb.search.api.entity.PostIdsBodyResponseNext;
import com.dnb.search.api.entity.PostIdsBodyResponseNextBody;
import com.dnb.search.api.entity.ReturnBody;
import com.dnb.search.api.service.ServiceablePassthru;
import com.dnb.search.api.service.count.CountPost;

public class RetrieveGet implements ServiceablePassthru{

	private static final Logger log = Logger.getLogger(CountPost.class);

	public RetrieveGet(){
		this.registerDao();
	}

	public void registerDao(){
		DaoFactory factory=DaoFactory.instance();
		if(!factory.isDaoRegistered(IdDaoGetImpl.class.getName())){
			DaoFactory.instance().registerDao(IdDaoGetImpl.class.getName(), new IdDaoGetImpl());
		}
	}

	public Response handlePassthruService(HttpServletRequest httpReq,
			HttpServletResponse httpResp, 
			AbstractDao idDaoGETImpl,
			String apiId,
			String params,
			String query,
			String scroll,
			String scrollid, 
			Integer pagesize) {

		String METHOD_NAME="RetrieveGet.handlePassthruService()";

		if(log.isDebugEnabled()){
			log.debug(CountPost.class.getName()+"."+METHOD_NAME+" START");
		}
		DaoFactory factory=DaoFactory.instance();
		if(null==idDaoGETImpl){
			idDaoGETImpl=factory.createProduct(IdDaoGetImpl.class.getName(), 
					httpReq, 
					httpResp, 
					apiId, 
					params, 
					null,
					scroll, 
					scrollid,
					pagesize);
		}
		ReturnBody returnBody=idDaoGETImpl.processSearch();
		String queryRan=returnBody.getQueryRan();
		if(null!=queryRan){
			//remove all extra white space
			queryRan=queryRan.replaceAll("\\s+", " ");
			//limit length to X_QUERY_MAX_LENGTH
			if(queryRan.length()>Constants.X_QUERY_MAX_LENGTH){
				queryRan=queryRan.substring(0,Constants.X_QUERY_MAX_LENGTH);
			}
		}

		try {
			if(returnBody.getStatusCode()==HttpServletResponse.SC_OK){
				JSONObject idsJSONObj=new JSONObject(returnBody.getResults());

				//There is an error, we have to return an error message
				if(idsJSONObj.has(Constants.ERROR_STR)){
					JSONObject errorsJSONObj=idsJSONObj.getJSONObject(Constants.ERROR_STR);
					int status=400;
					if(idsJSONObj.has(Constants.STATUS_STR)){
						status=idsJSONObj.getInt(Constants.STATUS_STR);
					}
					JSONArray errorsJSONArr=null;
					Error theErrors=new Error();
					if(errorsJSONObj.has(Constants.ROOT_CLAUSE_STR)){
						errorsJSONArr=errorsJSONObj.getJSONArray(Constants.ROOT_CLAUSE_STR);
						for(int i=0;i<errorsJSONArr.length();++i){
							JSONObject anError=errorsJSONArr.getJSONObject(i);
							theErrors.addError(anError.toString().replaceAll("\\\"", "\\'"));
						}
					}
					else{
						theErrors.addError(errorsJSONObj.toString());
					}

					//errorsJSONObj.get		
					return Response.status(status).
							entity(new GenericEntity<Error>(theErrors){}).
							header(Constants.X_QUERY_HEADER, queryRan).
							header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
							header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
							build();
				}

				if(idsJSONObj.has(Constants.HITS_STR)){
					JSONObject hitsJSONObj=idsJSONObj.getJSONObject(Constants.HITS_STR);
					int totalHits=hitsJSONObj.getInt(Constants.TOTAL_STR);
					//If we get here then there were no errors from the ES server
					PostIdsBodyResponse retBody=new PostIdsBodyResponse();
					//We have hits
					if(totalHits>0){

						//Although we have hits, we need to check to see if the scrolling is finished for the given scrollid
						boolean scrollIdIsStale=false;
						Boolean timedOut=idsJSONObj.getBoolean(Constants.TIMED_OUT_STR);
						if(timedOut){
							scrollIdIsStale=true;
						}
						//The scrollid has not yet timed_out, we need to check shard failures to be sure
						if(!scrollIdIsStale){
							JSONObject shardsJSONObj=idsJSONObj.getJSONObject(Constants._SHARDS_STR);
							if(shardsJSONObj.has(Constants.FAILURES_STR)){
								//there are failures in the shards JSONObject, but the hits show that there are results
								//this mean that the scrollid has already hit the end
								int failed=shardsJSONObj.getInt(Constants.FAILED_STR);
								JSONArray jsonArr=shardsJSONObj.getJSONArray(Constants.FAILURES_STR);
								if(failed>0||jsonArr.length()>0){
									scrollIdIsStale=true;
								}
							}
						}
						//The scrollid is no longer valid, we should output an error message
						if(scrollIdIsStale){
							Error error=new Error();
							error.addError(Constants.LHS_SCROLL_ID_NO_LONGER_VALID+" "+scrollid+" "+Constants.RHS_SCROLL_ID_NO_LONGER_VALID);;
							return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
									entity(new GenericEntity<Error>(error){}).
									header(Constants.X_QUERY_HEADER, queryRan).
									header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
									header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
									build();
						}

						if(hitsJSONObj.has(Constants.HITS_STR)){
							JSONArray hitsJSONArr=hitsJSONObj.getJSONArray(Constants.HITS_STR);
							List<String>hitsArray=new ArrayList<String>();

							for(int i=0;i<hitsJSONArr.length();++i){
								JSONObject jsonObj=hitsJSONArr.getJSONObject(i);
								if(jsonObj.has("fields")){
									//System.out.println("****This jsonObject is in the fields key: "+jsonObj.toString());
									//JSONObject dunsJSONObj=jsonObj.getJSONObject(Constants._SOURCE_STR);
									JSONObject dunsJSONObj=jsonObj.getJSONObject("fields");
									JSONArray dunsArray=dunsJSONObj.getJSONArray(Constants.DUNS_STR);
									String duns = dunsArray.getString(0);
									hitsArray.add(duns);	
								}
								else{
									log.debug(CountPost.class.getName()+"."+METHOD_NAME+
											  "This jsonObject is missing the fields key: "+
											  jsonObj.toString());
									if(jsonObj.has("_id")){
										JSONObject recordJSONObj=jsonObj.getJSONObject("_id");
										//JSONArray dunsArray=recordJSONObj.getJSONArray(Constants.DUNS_STR);
										String id = recordJSONObj.getString("_id");
										hitsArray.add(id);
									}
								}
							}
							retBody.setIds(hitsArray);

							PostIdsBodyResponseNextBody body=new PostIdsBodyResponseNextBody();

							String theScrollId=idsJSONObj.getString(Constants._SCROLL_ID_STR);
							body.setScrollid(theScrollId);
							retBody.setScrollid(theScrollId);
							int theTotal=hitsJSONObj.getInt(Constants.TOTAL_STR);
							retBody.setTotal(theTotal);
							PostIdsBodyResponseNext next=new PostIdsBodyResponseNext();
							AppIdHeader appid=new AppIdHeader();
							appid.setAppid(returnBody.getApiid());
							next.setHeader(appid);
							int paginationSize = Constants.DEFAULT_PAGE_SIZE;
							if(pagesize>0){
								if(theTotal>pagesize){
									paginationSize = pagesize;
								}
								else{
									paginationSize = theTotal;
								}
							}
							String nextUrl=(Constants.ADVANCED_GET_ID_WITH_SCROLL + "'" + theScrollId + "'");


							if(null!=scroll&&!scroll.isEmpty()){
								nextUrl+=(Constants.SCROLL_URL_STR + scroll);
							}
							next.setUrl(nextUrl);
							body.setScroll(scroll);
							next.setBody(body);

							retBody.setCount(hitsJSONArr.length());

							int took=idsJSONObj.getInt(Constants.TOOK_STR);
							retBody.setCount(hitsJSONArr.length());
							retBody.setNext(next);

							return Response.status(returnBody.getStatusCode()).
									entity(new GenericEntity<PostIdsBodyResponse>(retBody){}).
									header(Constants.X_QUERY_HEADER, queryRan).
									header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
									header(Constants.X_ES_TOOK_HEADER,took).
									build();

						}
						//We don't have the hits JSONArray, there is an error
						else{
							Error error=new Error();
							error.addError(Constants.NO_JSONARRAY_ERROR+", params="+params);
							return Response.status(HttpServletResponse.SC_NO_CONTENT).
									entity(new GenericEntity<Error>(error){}).
									header(Constants.X_QUERY_HEADER, queryRan).
									header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
									header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
									build();
						}
					}
					else{
						retBody.setCount(0);
						retBody.setIds(new ArrayList<String>());
						PostIdsBodyResponseNext next=new PostIdsBodyResponseNext();
						PostIdsBodyResponseNextBody body=new PostIdsBodyResponseNextBody();
						body.setScroll("");
						body.setScrollid("");

						next.setBody(body);
						AppIdHeader appid=new AppIdHeader();
						appid.setAppid(returnBody.getApiid());
						next.setHeader(appid);
						next.setUrl("");

						retBody.setTotal(0);
						retBody.setNext(next);

						return Response.status(returnBody.getStatusCode()).
								entity(new GenericEntity<PostIdsBodyResponse>(retBody){}).
								header(Constants.X_QUERY_HEADER, queryRan).
								header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
								header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
								build();
					}

				}
				//Response Body is Missing Hits
				else{
					log.error(CountPost.class.getName()+"."+METHOD_NAME+" END ");
					//					returnBody.addErrorMessage(Constants.NO_HITS_FOUND+" scroll="+scroll+" scollid="+scrollid+" appid="+apiId+" params="+params);
					Error error=new Error();
					error.addError(Constants.NO_HITS_FOUND+" scroll="+scroll+" scollid="+scrollid+" appid="+apiId+" params="+params);
					return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
							entity(new GenericEntity<Error>(error){}).
							header(Constants.X_QUERY_HEADER, queryRan).
							header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
							header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
							build();
				}
			}
			else{		
				//HTTP Servlet response != 200
				return Response.status(returnBody.getStatusCode()).
						entity(new GenericEntity<Error>(returnBody.getErrors()){}).
						header(Constants.X_QUERY_HEADER, queryRan).
						header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
						header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
						build();

			}
		} 
		catch (JSONException e) {
			//ES replied with a response in a bad Json format : Internal server error
			log.error(CountPost.class.getName()+"."+METHOD_NAME+" END "+e.getMessage());
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>(Constants.JSON_PARSE_ERROR){}).
					header(Constants.X_QUERY_HEADER, queryRan).
					header(Constants.X_API_ID_HEADER,returnBody.getApiid()).
					header(Constants.X_ES_TOOK_HEADER,returnBody.getTook()).
					build();
		}
	}


}




