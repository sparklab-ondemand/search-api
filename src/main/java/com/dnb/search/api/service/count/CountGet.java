package com.dnb.search.api.service.count;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.dao.AbstractDao;
import com.dnb.search.api.dao.CountDaoGetImpl;
import com.dnb.search.api.dao.DaoFactory;
import com.dnb.search.api.entity.AppIdHeader;
import com.dnb.search.api.entity.GetCountResponse;
import com.dnb.search.api.entity.ReturnBody;
import com.dnb.search.api.service.ServiceablePassthru;

public class CountGet implements ServiceablePassthru{

	private static final Logger log = Logger.getLogger(CountGet.class);

	public CountGet(){
		this.registerDao();
	}

	public void registerDao(){
		DaoFactory factory=DaoFactory.instance();
		if(!factory.isDaoRegistered(CountDaoGetImpl.class.getName())){
			DaoFactory.instance().registerDao(CountDaoGetImpl.class.getName(), new CountDaoGetImpl());
		}
	}

	public Response handlePassthruService(HttpServletRequest httpReq,
			HttpServletResponse httpResp, 
			AbstractDao countDaoGETImpl,
			String apiId,
			String params,
			String query,
			String scroll,
			String scrollid, 
			Integer pagesize) {

		String METHOD_NAME="CountGet.handlePassthruService()";

		if(log.isDebugEnabled()){
			log.debug(CountGet.class.getName()+"."+METHOD_NAME+" START");
		}
		ReturnBody retCount=null;
		DaoFactory factory=DaoFactory.instance();

		if(null==countDaoGETImpl){
			countDaoGETImpl=factory.createProduct(CountDaoGetImpl.class.getName(), 
					httpReq, 
					httpResp, 
					apiId, 
					null,
					params, 
					null, 
					null, 
					null);
		}
		retCount=countDaoGETImpl.processSearch();
		String queryRan=retCount.getQueryRan();
		if(null!=queryRan && !queryRan.isEmpty()){
			//remove all extra white space
			queryRan=queryRan.replaceAll("\\s+", " ");
			//limit length to X_QUERY_MAX_LENGTH
			if(queryRan.length()>Constants.X_QUERY_MAX_LENGTH){
				queryRan=queryRan.substring(0,Constants.X_QUERY_MAX_LENGTH);
			}
		}
		try {
			if(retCount.getStatusCode()==HttpServletResponse.SC_OK){
				JSONObject countJSONObj=new JSONObject(retCount.getResults());				
				//				JSONObject retJSONObj=new JSONObject();
				GetCountResponse responseObject = new GetCountResponse();
				responseObject.setTotal(0);
				responseObject.setQuery(queryRan);
				AppIdHeader apiHeader = new AppIdHeader();
				apiHeader.setAppid(retCount.getApiid());
				responseObject.setUrl(retCount.getUrl());
				responseObject.setHeader(apiHeader);

				if(countJSONObj.has(Constants.HITS_STR)){
					JSONObject firstHitJSONObj=countJSONObj.getJSONObject(Constants.HITS_STR);
					if(firstHitJSONObj.has(Constants.TOTAL_STR)){
						//						retJSONObj.put(Constants.TOTAL_STR, firstHitJSONObj.getInt(Constants.TOTAL_STR));
						responseObject.setTotal(firstHitJSONObj.getInt(Constants.TOTAL_STR));
					}
				}
				if(log.isDebugEnabled()){
					log.debug(CountGet.class.getName()+"."+METHOD_NAME+" END");
				}
				return Response.status(retCount.getStatusCode()).
						entity(new GenericEntity<GetCountResponse>(responseObject){}).
						header(Constants.X_QUERY_HEADER, queryRan).
						header(Constants.X_API_ID_HEADER,retCount.getApiid()).
						header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
						build();
			}
			else{
				String retMess=retCount.getReturnMessage().toString();
				log.error(CountGet.class.getName()+"."+METHOD_NAME+" END Error, statuscode="+
						retCount.getStatusCode()+" return message="+
						retMess);
				com.dnb.search.api.entity.Error error=new com.dnb.search.api.entity.Error();
				error.addError(retMess);
				return Response.status(retCount.getStatusCode()).
						entity(new GenericEntity<com.dnb.search.api.entity.Error>(error){}).
						header(Constants.X_QUERY_HEADER, queryRan).
						header(Constants.X_API_ID_HEADER,retCount.getApiid()).
						header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
						build();					
			}
		} 
		catch (JSONException e) {
			//e.printStackTrace();
			log.error(CountGet.class.getName()+"."+METHOD_NAME+" END "+e.getMessage());
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>(Constants.JSON_PARSE_ERROR){}).
					header(Constants.X_QUERY_HEADER, queryRan).
					header(Constants.X_API_ID_HEADER,retCount.getApiid()).
					header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
					build();
		}
	}
}


