package com.dnb.search.api.service.count;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.dao.AbstractDao;
import com.dnb.search.api.dao.CountDaoPostImpl;
import com.dnb.search.api.dao.DaoFactory;
import com.dnb.search.api.entity.AppIdHeader;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.PostCountBodyResponse;
import com.dnb.search.api.entity.PostCountBodyResponseBody;
import com.dnb.search.api.entity.ReturnBody;
import com.dnb.search.api.service.ServiceablePassthru;

public class CountPost implements ServiceablePassthru{

	private static final Logger log = Logger.getLogger(CountPost.class);

	public CountPost(){
		this.registerDao();
	}

	public void registerDao(){
		DaoFactory factory=DaoFactory.instance();
		if(!factory.isDaoRegistered(CountDaoPostImpl.class.getName())){
			DaoFactory.instance().registerDao(CountDaoPostImpl.class.getName(), new CountDaoPostImpl());
		}
	}

	public Response handlePassthruService(HttpServletRequest httpReq,
			HttpServletResponse httpResp, 
			AbstractDao countDaoPostImpl,
			String apiId,
			String params,
			String query,
			String scroll,
			String scrollid, 
			Integer pagesize) {

		String METHOD_NAME="handlePassthruService()";

		if(log.isDebugEnabled()){
			log.debug(CountPost.class.getName()+"."+METHOD_NAME+" START");
		}
		ReturnBody retCount=null;
		if(null==countDaoPostImpl){
			countDaoPostImpl=DaoFactory.instance().createProduct(CountDaoPostImpl.class.getName(), 
					httpReq, 
					httpResp, 
					apiId, 
					query, 
					query, 
					null, 
					null, 
					null);//new CountDaoPostImpl(httpReq, httpResp, apiId, params);
		}

		retCount=countDaoPostImpl.processSearch();
		String queryRan=retCount.getQueryRan();
		if(null!=queryRan){
			//remove all extra white space
			queryRan=queryRan.replaceAll("\\s+", " ");
			//limit length to X_QUERY_MAX_LENGTH
			if(queryRan.length()>Constants.X_QUERY_MAX_LENGTH){
				queryRan=queryRan.substring(0,Constants.X_QUERY_MAX_LENGTH);
			}
		}
		try {
			if(retCount.getStatusCode()==HttpServletResponse.SC_OK){
				JSONObject countJSONObj=new JSONObject(retCount.getResults());	

				//There is an error, we have to return an error message
				if(countJSONObj.has(Constants.ERROR_STR)){
					JSONObject errorsJSONObj=countJSONObj.getJSONObject(Constants.ERROR_STR);
					int status=400;
					if(countJSONObj.has(Constants.STATUS_STR)){
						status=countJSONObj.getInt(Constants.STATUS_STR);
					}
					JSONArray errorsJSONArr=null;
					Error theErrors=new Error();
					if(errorsJSONObj.has(Constants.ROOT_CLAUSE_STR)){
						errorsJSONArr=errorsJSONObj.getJSONArray(Constants.ROOT_CLAUSE_STR);
						for(int i=0;i<errorsJSONArr.length();++i){
							JSONObject anError=errorsJSONArr.getJSONObject(i);
							theErrors.addError(anError.toString().replaceAll("\\\"", "\\'"));
						}
					}
					else{
						theErrors.addError(errorsJSONObj.toString());
					}

					//errorsJSONObj.get		
					return Response.status(status).
							entity(new GenericEntity<Error>(theErrors){}).
							header(Constants.X_QUERY_HEADER, queryRan).
							header(Constants.X_API_ID_HEADER,retCount.getApiid()).
							header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
							build();
				}

				//If we get here then there were no errors from the ES server
				PostCountBodyResponse retBody=new PostCountBodyResponse();

				String baseQueryStr=this.getFetchIdsUrl(httpReq);
				if(baseQueryStr.endsWith("/")){
					baseQueryStr=baseQueryStr.substring(0,(baseQueryStr.length()-1));
				}					
				baseQueryStr+=("?"+Constants.DEFAULT_SCROLL_SUFFIX);

				retBody.setTotal(0);
				retBody.setUrl(baseQueryStr);
				AppIdHeader appHeader=new AppIdHeader();
				appHeader.setAppid(retCount.getApiid());				
				retBody.setHeader(appHeader);

				PostCountBodyResponseBody body=new PostCountBodyResponseBody();

				body.setQuery(queryRan);				
				body.setSize(Constants.DEFAULT_PAGE_SIZE);				
				retBody.setBody(body);

				if(countJSONObj.has(Constants.TOOK_STR)){
					retCount.setTook(countJSONObj.getInt(Constants.TOOK_STR));					
				}

				if(countJSONObj.has(Constants.HITS_STR)){
					JSONObject firstHitJSONObj=countJSONObj.getJSONObject(Constants.HITS_STR);
					if(firstHitJSONObj.has(Constants.TOTAL_STR)){	
						int total=firstHitJSONObj.getInt(Constants.TOTAL_STR);						
						retBody.setTotal(total);						
						//If the total is 0 then we want to make the url empty
						if(0==total){
							retBody.setUrl("");
							body.setSize(0);
						}
						else if(total<1000){
							body.setSize(total);
						}
					}
				}

				//				return Response.status(retCount.getStatusCode()).
				//						entity(new GenericEntity<PostCountBodyResponse>(retBody){}).
				//						header(Constants.X_QUERY_HEADER, queryRan).
				//						header(Constants.X_API_ID_HEADER,retCount.getApiid()).
				//						header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
				//						build();
				//Do not use 
				JSONObject retJSON=new JSONObject();
				retJSON.put(Constants.TOTAL_STR, retBody.getTotal());
				retJSON.put(Constants.URL_STR, retBody.getUrl());
				retJSON.put(Constants.HEADER_STR, retBody.getHeaderJSONObject());
				retJSON.put(Constants.BODY_STR, retCount.getJsonQuery());

				return Response.ok(retJSON.toString(), MediaType.APPLICATION_JSON).
						header(Constants.X_QUERY_HEADER, queryRan).
						header(Constants.X_API_ID_HEADER,retCount.getApiid()).
						header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
						build();
			}
			else{
				Error error=new Error();
				error.addError(retCount.getReturnMessage().toString());
				return Response.status(retCount.getStatusCode()).
						entity(new GenericEntity<Error>(error){}).
						header(Constants.X_QUERY_HEADER, queryRan).
						header(Constants.X_API_ID_HEADER,retCount.getApiid()).
						header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
						build();					
			}
		} 
		catch (JSONException e) {
			//e.printStackTrace();
			log.error(CountPost.class.getName()+"."+METHOD_NAME+" END "+e.getMessage());
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>(Constants.JSON_PARSE_ERROR){}).
					header(Constants.X_QUERY_HEADER, queryRan).
					header(Constants.X_API_ID_HEADER,retCount.getApiid()).
					header(Constants.X_ES_TOOK_HEADER,retCount.getTook()).
					build();
		}
	}

	private String getBaseUrl(HttpServletRequest httpReq){
		String retVal="";
		if(null!=httpReq){
			String reqURI=httpReq.getRequestURI();
			int index=reqURI.indexOf(Constants.END_POINT_LHS_DELIMETER);
			if(-1!=index){
				index+=(Constants.END_POINT_LHS_DELIMETER.length());
				retVal=reqURI.substring(index);
			}
			index=retVal.lastIndexOf(Constants.COUNT_STR);
			if(-1!=index){
				retVal=retVal.substring(0,index);
			}			
		}
		return retVal;
	}

	private String getFetchIdsUrl(HttpServletRequest httpReq){
		String retVal="";
		if(null!=httpReq){
			retVal=this.getBaseUrl(httpReq);
			if(null!=retVal && !retVal.isEmpty()){
				if(!retVal.endsWith("/")){
					retVal+="/";
				}
				retVal+=Constants.FETCH_IDS_URL_SUFFIX;
				String queryStr=httpReq.getQueryString();
				if(null!=queryStr && !queryStr.trim().isEmpty()){
					retVal+=("?"+queryStr);
				}
			}
			else{
				retVal="";
			}
		}
		return retVal;
	}

}
