package com.dnb.search.api.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.WhereClause;
import com.dnb.search.api.service.count.CountGet;
import com.dnb.search.api.service.count.CountPost;
import com.dnb.search.api.service.record.RetrieveGet;
import com.dnb.search.api.service.record.RetrievePost;
import com.dnb.search.api.util.SearchServiceUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;


/*******
 * 
 * @author doanth
 *
 * This is the Restful point for the dynamoDB queries. This API uses swagger for it's documentation. The swagger UI is outo f the box
 * and is installed as part of the service. The base Url for it is: localhost:8080/dx-api/
 * 
 */

@Path("/search")
@Api(
		value="Search API",
		description="DnB RESTful service for search"
		)
@Produces({"application/json"})
public class SearchService {

	static{
		//This enables logging at build time
		BasicConfigurator.configure();
	}

	private static Logger log = Logger.getLogger(SearchService.class);

	
	
	/**********
	 * COUNTER specific endpoints START
	 **********/

	@GET
	@Path("advanced/count/query/{query}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({"application/json"})
	@ApiOperation( httpMethod="GET",
	value = "Given an Elasticsearch query string, return the count that matches that query clause", 
	notes = "Given an Elasticsearch query string, return the count that matches that query clause")
	@ApiResponses(value={
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code=401,message="Unauthorized id"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response countGET(@Context HttpServletRequest httpReq,			
                             @Context HttpServletResponse httpResp,
	                         
                             @HeaderParam(value=Constants.X_API_ID_HEADER)String apiId,
                             
                             @ApiParam(value = "Elasticsearch query string to filter search for example state:texas", required = true) 
	                         @PathParam("query")String query)
	{
//		String METHOD_NAME="getCount()";
//		if(log.isDebugEnabled()){
//			log.debug(this.getClass().getName()+": "+METHOD_NAME+": START:");			
//		}

		//We need to register the CountDaoGetImpl, call its constructor
		CountGet servicePassthru=new CountGet();
		return servicePassthru.handlePassthruService(httpReq, httpResp,null, apiId, query,null,null,null,null);
	}


	
	@POST
	@Path("advanced/count")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation( httpMethod="POST",			       
	               value = "Given an APP ID and an elastic DSL query, return the count that matches the DSL query", 
	               notes = "Given an APP ID and an elastic DSL query, return the count that matches the DSL query")
	@ApiResponses(value={ 
			             @ApiResponse(code=200,message="Everything's OK"),
			             @ApiResponse(code=400,message="Bad request"),
			             @ApiResponse(code=401,message="Unauthorized id"),
			             @ApiResponse(code = 500, message = "Server error")
	}) 
	public Response countPOST(@Context HttpServletRequest httpReq, 
			                  @Context HttpServletResponse httpResp,
			                  @HeaderParam(value=Constants.X_API_ID_HEADER)String apiId,
			                  @ApiParam(value="Elasticsearch query DSL, for example: {\"query\":{\"term\":{\"siteCity\":\"austin\"}}} ", required=true) String query){			                  	
		
		CountPost servicePassthru=new CountPost();
		return servicePassthru.handlePassthruService(httpReq, httpResp,null, apiId,null,query,null,null,null);	
	}	

	/**********
	 * COUNTER specific endpoints END
	 **********/
	
	/**********
	 * FETCH specific endpoints START
	 **********/

	@GET
	@Path("advanced/ids/query/{query}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({"application/json"})
	@ApiOperation( httpMethod="GET",
	value = "Given a elastic search query clause, return the count that matches that query clause", 
	notes = "Given a elastic search query clause, return the count that matches that query clause")
	
	@ApiResponses(value={
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code=401,message="Unauthorized id"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response fetchRecordsGET(@Context HttpServletRequest httpReq,
			                        @Context HttpServletResponse httpResp,
			                        
			                        @ApiParam(value="optional scroll timeout value, for example: 2m")
	                                @QueryParam(value="scroll")String scroll,

			                        @ApiParam(value="optional scrollid used to retrieve next page size, if present then query is ignored")
	                                @QueryParam(value="scrollid")String scrollid,	                  
	                                
			                        @ApiParam(value="optional page size")
	                                @QueryParam(value="pagesize")int size,	                  	                                


	                                @HeaderParam(value=Constants.X_API_ID_HEADER)String apiId,
	                                
                                    @ApiParam(value="Elasticsearch query clause to filter search for example: state:texas", required=true) 
                                    @PathParam("query")String query)
	{
		RetrieveGet service=new RetrieveGet();
		return service.handlePassthruService(httpReq, httpResp, null, apiId, query,null,scroll, scrollid, size);
	}

	
	@POST
	@Path("advanced/ids")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation( httpMethod="POST",			       
	value = "Given an APP ID and an elastic DSL query, return the count that matches the DSL query", 
	notes = "Given an APP ID and an elastic DSL query, return the count that matches the DSL query")
	@ApiResponses(value={ 
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code=401,message="Unauthorized id"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response fetchRecordsPOST(@Context HttpServletRequest httpReq, 
			                         @Context HttpServletResponse httpResp,
			                     
				                     @ApiParam(value="optional scroll timeout value, for example: 2m")
		                             @QueryParam(value="scroll")String scroll,

				                     @ApiParam(value="optional scrollid used to retrieve next page size, if present, then the query is ignored")
		                             @QueryParam(value="scrollid")String scrollid,	                  
		                                
				                     @ApiParam(value="optional page size")
		                             @QueryParam(value="pagesize")int size,			                         
			                         
			                         @HeaderParam(value=Constants.X_API_ID_HEADER) String apiId,
			                         
			                         @ApiParam(value="Elasticsearch query DSL, for example: {\"query\":{\"term\":{\"siteCity\":\"austin\"}}} ", required=true) String query){
		RetrievePost servicePassthru=new RetrievePost();		
		return servicePassthru.handlePassthruService(httpReq, httpResp,null, apiId, null, query, scroll, scrollid, size);
		
	}	

	/**********
	 * Fetch endpoints END
	 **********/
	
	/*********
	 * SQL Endpoints START
	 *********/

	@GET
	@Path(Constants.SQL_COUNT_GET+"{clause}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation( httpMethod="GET",			       
	value = "Use sql syntax to do elasticsearch count", 
	notes = "SQL Where clause"
			)
	@ApiResponses(value={ 
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response sqlSearchGet(@Context HttpServletRequest httpReq,
			                      @Context HttpServletResponse httpResp,
			                      
		                          @HeaderParam(value=Constants.X_API_ID_HEADER)String apiId,
		                             
		                          @ApiParam(value = "SQL where clause, for example: siteCity='Austin' AND state='texas' ", required = true) 
			                      @PathParam("clause")String clause){
		
		return SearchServiceUtil.handleSQLGetCount(httpReq, httpResp, clause, apiId);
	}
	
	@POST
	@Path(Constants.SQL_COUNT_POST)
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation( httpMethod="POST",			       
	value = "Use sql syntax to do elasticsearch count", 
	notes = "SQL Where clause"
			)
	@ApiResponses(value={ 
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response sqlSearchPost(@Context HttpServletRequest httpReq,
			                      @Context HttpServletResponse httpResp,
			                      @HeaderParam(value=Constants.X_API_ID_HEADER) String apiId,
			                      @ApiParam(value="<span style='font-weight:bold;'>Required: SQL WHERE clause</span> for example: {\"clause\":\"siteCity='Austin' OR siteCity='Boston'\"}", required=true) WhereClause clause){
		
		//The first thing we should do is make sure that the clause is valid JSON
		if(null==clause||null==clause.getClause()||clause.getClause().isEmpty()){
			//We return an error message
			Error errors=new Error();
			errors.addError(Constants.QUERY_CLAUSE_REQUIRED);
			
			return Response.status(HttpServletResponse.SC_BAD_REQUEST).
			       entity(new GenericEntity<Error>(errors){}).
			       header(Constants.X_API_ID_HEADER,apiId).
			       build();
		}		
		return SearchServiceUtil.handleSQLPostCount(httpReq, httpResp, clause, apiId);
		
	}
	
	@GET
	@Path(Constants.SQL_IDS_GET+"{clause}")
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({"application/json"})
	@ApiOperation( httpMethod="GET",
	value = "Given an APP ID and SQL WHERE clause, retrieve the duns ids matching the specified criteria", 
	notes = "Given an APP ID and SQL WHERE clause, retrieve the duns ids matching the specified criteria")
	
	@ApiResponses(value={
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code=401,message="Unauthorized id"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response sqlIdsGet(@Context HttpServletRequest httpReq,
			                        @Context HttpServletResponse httpResp,
			                        
			                        @ApiParam(value="optional scroll timeout value")
	                                @QueryParam(value="scroll")String scroll,

			                        @ApiParam(value="optional scrollid used to retrieve next page size, if present then query is ignored")
	                                @QueryParam(value="scrollid")String scrollid,	                  
	                                
			                        @ApiParam(value="optional page size")
	                                @QueryParam(value="pagesize")int size,	                  	                                

	                                @HeaderParam(value=Constants.X_API_ID_HEADER)String apiId,
	                                
                                    @ApiParam(value="SQL where clause to filter elastic search for example: siteCity='Austin' OR siteCity='Boston'", required=true) 
                                    @PathParam("clause")String clause){
		return SearchServiceUtil.handleSQLGetIds(httpReq, httpResp, clause, apiId, scroll, scrollid, size);
	}
	
	@POST
	@Path(Constants.SQL_IDS_POST)
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@ApiOperation( httpMethod="POST",			       
	value = "Given an APP ID and SQL WHERE clause, retrieve the duns ids matching the specified criteria", 
	notes = "Given an APP ID and SQL WHERE clause, retrieve the duns ids matching the specified criteria"
			)
	@ApiResponses(value={ 
			@ApiResponse(code=200,message="Everything's OK"),
			@ApiResponse(code=400,message="Bad request"),
			@ApiResponse(code = 500, message = "Server error")
	}) 
	public Response sqlIdsPost(@Context HttpServletRequest httpReq,
			                   @Context HttpServletResponse httpResp,
			                      
				               @ApiParam(value="optional scroll timeout value")
		                       @QueryParam(value="scroll")String scroll,

				               @ApiParam(value="optional scrollid used to retrieve next page size, if present, then the query is ignored")
		                       @QueryParam(value="scrollid")String scrollid,	                  
		                                
				               @ApiParam(value="optional page size")
		                       @QueryParam(value="pagesize")int size,			                         
			                         
			                   @HeaderParam(value=Constants.X_API_ID_HEADER) String apiId,			                      
			                      
			                   @ApiParam(value="<span style='font-weight:bold;'>Required: SQL WHERE clause</span> for example: {\"clause\": \"siteCity='Austin' OR siteCity='Boston'\"}", required=true) WhereClause clause){
		
		//The first thing we should do is make sure that the clause is valid JSON
		if((null==clause||null==clause.getClause()||clause.getClause().isEmpty()) && (null==scrollid||scrollid.isEmpty())){
			//We return an error message
			Error errors=new Error();
			errors.addError(Constants.QUERY_CLAUSE_OR_SROLL_ID_REQUIRED);
			
			return Response.status(HttpServletResponse.SC_BAD_REQUEST).
			       entity(new GenericEntity<Error>(errors){}).
			       header(Constants.X_API_ID_HEADER,apiId).
			       build();
		}

		return SearchServiceUtil.handleSQLPostIds(httpReq, httpResp, clause, apiId, scroll, scrollid, size);
	}
	
	/*********
	 * SQL Endpoints END
	 *********/
	
}
