package com.dnb.search.api.service.meta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.dnb.search.api.entity.Parameters;
import com.dnb.search.api.service.Serviceable;

public class MetaGet implements Serviceable{

	public Response handleService(HttpServletRequest request,
			HttpServletResponse response, Parameters params) {

		return Response.status(HttpServletResponse.SC_OK).
				entity(new GenericEntity<String>("Invoked healthCheck()"){}).
				//header(Constants.MONETIZATION_HTTP_HEADER_NAME, this.getMonetizationStr()).
				build();
	}

	
}
