package com.dnb.search.api.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import com.dnb.search.api.entity.Parameters;

public interface Serviceable {
	public Response handleService(HttpServletRequest request, HttpServletResponse response, Parameters params);
}
