package com.dnb.search.api.service
;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import com.dnb.search.api.dao.AbstractDao;

public interface ServiceablePassthru {

	public Response handlePassthruService(HttpServletRequest request, 
			                              HttpServletResponse response, 
			                              AbstractDao idao,
			                              String apiId,
			                              String params,
			                              String query,
			                              String scroll,
			                              String scrollid, 
			                              Integer pagesize);
	public void registerDao();
}
