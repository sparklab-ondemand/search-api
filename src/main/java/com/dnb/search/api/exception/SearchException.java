package com.dnb.search.api.exception;

public class SearchException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2688071662513711146L;

	public SearchException(String message){
		super(message);
	}
	
	public String getMessage(){
		return super.getMessage();
	}

}
