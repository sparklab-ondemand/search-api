package com.dnb.search.api.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.json.JSONObject;

import com.dnb.search.api.Constants;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "total",
	    "url",
	    "header",
	    "body"
	})
public class PostCountBodyResponse {
	
	private int total;
	private String url;
	private AppIdHeader header;
	private PostCountBodyResponseBody body;
	
	@XmlElement(name="total")
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
	@XmlElement(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@XmlElement(name="body")
    public PostCountBodyResponseBody getBody() {
		return body;
	}
	public void setBody(PostCountBodyResponseBody body) {
		this.body = body;
	}
	
	@XmlElement(name="header")
	public AppIdHeader getHeader() {
		return header;
	}
	public void setHeader(AppIdHeader header) {
		this.header = header;
	}
	
	public JSONObject getHeaderJSONObject(){
		JSONObject retVal=new JSONObject();
		if(null!=this.header && null!=this.header.getAppid()){
			retVal.put(Constants.X_API_ID_HEADER, this.header.getAppid());
		}
		else{
			retVal.put(Constants.HEADER_STR, JSONObject.NULL);
		}
		return retVal;
	}
}
