package com.dnb.search.api.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.dnb.search.api.Constants;

@SuppressWarnings("restriction")
@XmlRootElement
public class Error implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -94405491531176423L;
	private static final Logger log = Logger.getLogger(Error.class);

	
	private List<String>errors;

	public Error(){
		this.errors=new ArrayList<String>();	
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public void addError(String anError){
		this.errors.add(anError);
	}
	
	public boolean hasErrors(){
		boolean retVal=false;
		if(null!=this.errors){
			retVal=(this.errors.size()>0);
		}
		return retVal;
	}
	
	public int getNumberOfErrors(){
		return this.errors.size();
	}
	
	public JSONObject toJSON(){
		String METHOD_NAME="toJSON()";
		JSONObject retVal=new JSONObject();
		JSONArray retArr=new JSONArray();
		try {
			retVal.put(Constants.ERRORS_STR, retArr);
			for(String anError:this.errors){
			  retArr.put(anError);
			}
		} 
		catch (JSONException e) {
			log.error(Error.class.getName()+"."+METHOD_NAME+": "+Constants.JSON_PARSE_ERROR);
			//e.printStackTrace();
		}
		return retVal;
	}
}
