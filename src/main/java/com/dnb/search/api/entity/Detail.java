package com.dnb.search.api.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("restriction")
@XmlRootElement
public class Detail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5093074257883309758L;
	private String detail;

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}	
}
