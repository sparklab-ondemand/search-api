package com.dnb.search.api.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "total",
	    "count",
	    "ids",
	    "scrollid",
	    "header",
	    "next"
	})
public class GetIDResponse {
	
	private int total;
	private int count;
	private List<String>ids;
	private AppIdHeader header;
	private String scrollid;
	private String next;
	
	@XmlElement(name="header")
	public AppIdHeader getHeader() {
		return header;
	}
	public void setHeader(AppIdHeader header) {
		this.header = header;
	}
	
	@XmlElement(name="total")
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
	@XmlElement(name="count")
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="ids")
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	
	@XmlElement(name="scrollid")
	public String getScrollid() {
		return scrollid;
	}
	public void setScrollid(String scrollid) {
		this.scrollid = scrollid;
	}
	
	@XmlElement(name="next")
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
}
