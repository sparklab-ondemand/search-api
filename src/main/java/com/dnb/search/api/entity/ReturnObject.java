package com.dnb.search.api.entity;

import org.json.JSONObject;

public class ReturnObject {
	
	private String apiid;
	private String queryRan;
	private int took;
	private int statusCode;
	private Long startProcessTime;
	private Long endProcessTime;
	private JSONObject returnMessage;
	private Error errors;
	
	public void setStartProcessTime(Long startProcessTime) {
		this.startProcessTime = startProcessTime;
	}
	public void setEndProcessTime(Long endProcessTime) {
		this.endProcessTime = endProcessTime;
	}
	
	public String getQueryRan() {
		return queryRan;
	}
	public void setQueryRan(String queryRan) {
		this.queryRan = queryRan;
	}
	public String getApiid() {
		return apiid;
	}
	public void setApiid(String apiid) {
		this.apiid = apiid;
	}	
	public int getTook() {
		return took;
	}
	public void setTook(int took) {
		this.took = took;
	}

	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public JSONObject getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(JSONObject returnMessage) {
		this.returnMessage = returnMessage;
	}
	
	public Error getErrors() {
		return errors;
	}
	public void setErrors(Error errors) {
		this.errors = errors;
	}
	public Long getStartProcessTime() {
		return startProcessTime;
	}
	public Long getEndProcessTime() {
		return endProcessTime;
	}
	public Long getApiProcessTime(){
	    if(null!=this.startProcessTime && null!=this.endProcessTime){
	    	return this.endProcessTime-this.startProcessTime;
	    }
	    else if(null!=this.startProcessTime){
	        Long currentTime=System.currentTimeMillis();
	        return (currentTime-this.startProcessTime);
	    }
	    return -1L;
	}
}
