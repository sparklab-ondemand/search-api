package com.dnb.search.api.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "total",
	    "count",
	    "ids",
	    "scrollid",
	    "next"
	})
public class PostIdsBodyResponse {
	
	private int total;
	private int count;
	private List<String>ids;
	private String scrollid;
	private PostIdsBodyResponseNext next;
	
	
	@XmlElement(name="total")
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
	@XmlElement(name="count")
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	@XmlElement(name="ids")
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	
	@XmlElement(name="scrollid")
	public String getScrollid() {
		return scrollid;
	}
	public void setScrollid(String scrollid) {
		this.scrollid = scrollid;
	}
	
	@XmlElement(name="next")
	public PostIdsBodyResponseNext getNext() {
		return next;
	}
	public void setNext(PostIdsBodyResponseNext next) {
		this.next = next;
	}
}
