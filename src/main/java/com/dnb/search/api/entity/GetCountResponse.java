package com.dnb.search.api.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "total",
	    "url",
	    "header",
	    "query"
	})

public class GetCountResponse {
	
	private int total;
	private String url;
	private AppIdHeader header;
	private String query;
	
	@XmlElement(name="total")
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	@XmlElement(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@XmlElement(name="header")
	public AppIdHeader getHeader() {
		return header;
	}
	public void setHeader(AppIdHeader header) {
		this.header = header;
	}
	@XmlElement(name="query")
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}

}
