package com.dnb.search.api.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "scroll",
	    "scrollid"
	})
public class PostIdsBodyResponseNextBody {
	
	String scroll;
	String scrollid;
	
	@XmlElement(name="scroll")
	public String getScroll() {
		return scroll;
	}
	public void setScroll(String scroll) {
		this.scroll = scroll;
	}
	
	@XmlElement(name="scrollid")
	public String getScrollid() {
		return scrollid;
	}
	public void setScrollid(String scrollid) {
		this.scrollid = scrollid;
	}
	
	

}
