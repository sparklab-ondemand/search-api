package com.dnb.search.api.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@SuppressWarnings("restriction")
@XmlRootElement
@JsonSerialize
@XmlType(propOrder = {
	    "apiid",
	    "query"
	})
public class SearchPassthruParameters implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4863758703108787222L;
	
	private String apiid;
	private String query;
	
	@XmlElement(name="apiid")
	public String getApiid() {
		return apiid;
	}
	public void setApiid(String apiid) {
		this.apiid = apiid;
	}
	
	@XmlElement(name="query")
	@JsonFormat
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}

}
