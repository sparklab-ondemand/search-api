package com.dnb.search.api.entity;



public class ReturnCount extends ReturnObject{
	
	private String results;
	private String queryRan;
	private String url;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private int took;
	private int apiTook;
	
	
	public String getResults() {
		return results;
	}
	public void setResults(String results) {
		this.results = results;
	}
	public String getQueryRan() {
		return queryRan;
	}
	public void setQueryRan(String queryRan) {
		this.queryRan = queryRan;
	}
	public int getTook() {
		return took;
	}
	public void setTook(int took) {
		this.took = took;
	}
	public int getApiTook() {
		return apiTook;
	}
	public void setApiTook(int apiTook) {
		this.apiTook = apiTook;
	}
	
	
}
