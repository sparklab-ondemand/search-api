package com.dnb.search.api.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "url",
	    "header",
	    "body"
	})
public class PostIdsBodyResponseNext {
	
	private String url;
	private AppIdHeader header;
	private PostIdsBodyResponseNextBody body;
	
	@XmlElement(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@XmlElement(name="header")
	public AppIdHeader getHeader() {
		return header;
	}
	public void setHeader(AppIdHeader header) {
		this.header = header;
	}
	
	@XmlElement(name="body")
	public PostIdsBodyResponseNextBody getBody() {
		return body;
	}
	public void setBody(PostIdsBodyResponseNextBody body) {
		this.body = body;
	}
}
