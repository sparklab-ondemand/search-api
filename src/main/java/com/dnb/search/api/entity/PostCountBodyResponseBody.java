package com.dnb.search.api.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "query",
	    "size"
	})
public class PostCountBodyResponseBody {
	
	private String query;
	private int size;
	
	@XmlElement(name="query")
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	
	@XmlElement(name="size")
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
}
