package com.dnb.search.api.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@XmlRootElement
@XmlType(propOrder = {"clause"})
public class WhereClause implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7870678012279967454L;
	
	private String clause;

	@XmlElement(name="clause")
	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}
	
	public String toString(){
		JSONObject retVal=new JSONObject();
		try {
			retVal.put("clause", this.clause);
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retVal.toString();
	}
}
