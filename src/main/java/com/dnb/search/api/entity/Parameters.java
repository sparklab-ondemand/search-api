package com.dnb.search.api.entity;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlType(propOrder = {
	    "apiid",
	    "clause"
	})
public class Parameters implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1273259672902166784L;
	
	private String apiid;
	private String clause;
	
	@XmlElement(name="apiid")
	public String getApiid() {
		return apiid;
	}
	public void setApiid(String apiid) {
		this.apiid = apiid;
	}
	
	@XmlElement(name="clause")
	public String getClause() {
		return clause;
	}
	public void setClause(String clause) {
		this.clause = clause;
	}
	
	

}
