package com.dnb.search.api.entity;

import org.json.JSONObject;


public class ReturnBody extends ReturnObject{
	
	private String results;
	private String url;
	private String scroll;
	private Integer size;
	private JSONObject jsonQuery;
	
	public ReturnBody(){
		super();
		super.setStartProcessTime(System.currentTimeMillis());
		super.setErrors(new Error());
	}

	public void addErrorMessage(String errMessage){
	    super.getErrors().addError(errMessage);	
	}
	
	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}

	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public JSONObject getJsonQuery() {
		return jsonQuery;
	}

	public void setJsonQuery(JSONObject jsonQuery) {
		this.jsonQuery = jsonQuery;
	}
	
	
	
}
