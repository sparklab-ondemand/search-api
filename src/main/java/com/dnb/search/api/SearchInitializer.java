package com.dnb.search.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

public class SearchInitializer {

	private static final Logger log = Logger.getLogger(SearchInitializer.class);

	private static Set<String>apiIds;
	private static String elasticSearchUrl;

	private static String elasticSearchBaseUrl;
	private static String searchIndex;
	private static String elasticSearchHostName;



	static{
		init();	
	}

	public void reload(){
		synchronized(this){
			SearchInitializer.init();
		}
	}

	private static String init(){		
		String retVal="init()";
		
		Properties searchProps=new Properties();
		InputStream inny=null;
		String searchHostName=null;
		try {
			//load the resources/search.properties file from the project so we can use it if needed 
			inny=SearchInitializer.class.getClassLoader().getResourceAsStream(Constants.SEARCH_API_PROPS_FILE);
			//Add for local testing
			if(null==inny){
				inny=new FileInputStream(new File("./src/main/resources/search.properties"));
			}
			searchProps.load(inny);
			
			//Constants.CATAINA_SEARCH_HOST_PROP_NAME should be set on the tomcat server in conf/catalina.properties file 
			searchHostName=System.getProperty(Constants.CATAINA_SEARCH_HOST_PROP_NAME);

			if(null==searchHostName && null!=searchProps){
				searchHostName=searchProps.getProperty(Constants.CATAINA_SEARCH_HOST_PROP_NAME,Constants.SEARCH_HOST_NAME);
			}
			if(null!=searchHostName && !searchHostName.endsWith("/")){
				searchHostName+="/";			
			}
			if(null==searchHostName){
				searchHostName="/";
			}
			//This the base url without the search Index, for example: http://localhost:9200/
			SearchInitializer.elasticSearchBaseUrl=new String(searchHostName);
			//First look in the conf/catalina.properties to find the SEARCH_INDEX property value
			SearchInitializer.searchIndex=System.getProperty(Constants.CATALINA_SEARCH_INDEX_PROP_NAME);
			//If the value is null then try looking in the resources/search.properties file
			if(null==SearchInitializer.searchIndex && null!=searchProps){
				SearchInitializer.searchIndex=searchProps.getProperty(Constants.CATALINA_SEARCH_INDEX_PROP_NAME, Constants.SEARCH_INDEX_NAME);
			}		 

			SearchInitializer.elasticSearchHostName = searchHostName;
			searchHostName+=SearchInitializer.searchIndex;
			
			if(!SearchInitializer.elasticSearchHostName.endsWith("/")){
				SearchInitializer.elasticSearchHostName+="/";
			}
			if(!searchHostName.endsWith("/")){
				searchHostName+="/";			
			}			
			//Initialize the apiIds set		
			SearchInitializer.apiIds=new HashSet<String>();
			SearchInitializer.initializeApiIds(searchProps);
		} 
		catch (IOException e) {
			log.debug("ERROR: Could not load "+Constants.SEARCH_API_PROPS_FILE+" file setting searchProps to null value and moving on");
			searchProps=null;
		}
		finally{
			if(null!=inny){
				try {
					//Make sure we close the InputStream
					inny.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		SearchInitializer.elasticSearchUrl=searchHostName;
		
		
		return retVal;		
	}

	public static String getElasticSearchUrl(){
		return SearchInitializer.elasticSearchUrl;
	}
	
	public static String getElastSearchBaseUrl(){
		return SearchInitializer.elasticSearchBaseUrl;
	}
	
	public static String getSearchIndex(){
		return SearchInitializer.searchIndex;

	}
	
	public static String getElasticSearchHostName(){
		return SearchInitializer.elasticSearchHostName;

	}

	public static boolean doesApiIdExist(String apiId){
		boolean retVal=false;
		if(null!=apiId){
			retVal=SearchInitializer.apiIds.contains(apiId);
		}
		return retVal;
	}

	private static void initializeApiIds(Properties searchProps){
		if(null!=searchProps) {
			//apiidsProps.getProperty(Constants.API_IDS_KEY, Constants.DEFAULT_API_IDS);
			String apiIds=System.getProperty(Constants.API_IDS_KEY);
			if(null==apiIds || apiIds.isEmpty()){
				apiIds=searchProps.getProperty(Constants.API_IDS_KEY,Constants.DEFAULT_API_IDS);
			}
			if(null!=apiIds && apiIds.length()>0){
				String[]apiIdsArray=apiIds.split(Constants.API_ID_DELIMETER);
				if(null!=apiIdsArray){
					for(String anApiId:apiIdsArray){
						SearchInitializer.apiIds.add(anApiId);
					}
				}
			}
		} 
	}
}
