package com.dnb.search.api.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.WhereClause;
import com.dnb.search.api.service.count.CountPost;
import com.dnb.search.api.service.record.RetrievePost;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;

public class SearchServiceUtil {

	public static String callElasticSearchSqlPlugin(String url, String selectStatement) throws IOException{

		Builder builder=new Request.Builder();
		builder
		.header("ACCEPT", "application/json")
		.url(url);

		RequestBody body=RequestBody.create(com.squareup.okhttp.MediaType.parse("application/json"),selectStatement);
		builder.post(body);

		Request request=builder.build();
		OkHttpClient httpClient=new OkHttpClient();	
		com.squareup.okhttp.Response response=httpClient.newCall(request).execute();

		String retVal=response.body().string();

		return retVal;
	}

	public static Response handleSQLGetCount(HttpServletRequest httpReq, 
			HttpServletResponse httpResp, 
			String clause,
			String apiId){
		WhereClause whereClause=new WhereClause();
		whereClause.setClause(clause);
		return SearchServiceUtil.handleSQLPostCount(httpReq, 
				httpResp, 
				whereClause,
				apiId);		
	}

	public static Response handleSQLPostCount(HttpServletRequest httpReq, 
			HttpServletResponse httpResp, 
			WhereClause clause,
			String apiId){
		String theClause=clause.getClause();

		String selectStatement=Constants.SELECT_STAR_STR+SearchInitializer.getSearchIndex()+" WHERE ";

		String url=SearchInitializer.getElastSearchBaseUrl();
		if(!url.endsWith("/")){
			url+="/";
		}
		url+=Constants.EXPLAIN_URL_SUFFIX;

		String respStr="";
		try {
			if(theClause!=null){
				selectStatement+=URLDecoder.decode(theClause, Constants.UTF_STR);
				respStr=SearchServiceUtil.callElasticSearchSqlPlugin(url, selectStatement);
				if(null!=selectStatement && !selectStatement.trim().isEmpty()){
					//remove all extra white space
					selectStatement=selectStatement.replaceAll("\\s+", " ");
					//limit length to X_QUERY_MAX_LENGTH for putting in the header
					if(selectStatement.length()>Constants.X_QUERY_MAX_LENGTH){
						selectStatement=selectStatement.substring(0,Constants.X_QUERY_MAX_LENGTH);
					}
				}
				JSONObject respJSON=new JSONObject(respStr);

				if(respJSON.has(Constants.ERROR_STR)){
					Error errors=new Error();
					JSONObject errorsJSON=respJSON.getJSONObject(Constants.ERROR_STR);
					if(errorsJSON.has(Constants.ROOT_CLAUSE_STR)){
						JSONArray rootCauseJSONArr=errorsJSON.getJSONArray(Constants.ROOT_CLAUSE_STR);

						for(int i=0;i<rootCauseJSONArr.length();++i){
							JSONObject aRootCauseJSONObj=rootCauseJSONArr.getJSONObject(i);
							errors.addError(aRootCauseJSONObj.getString(Constants.TYPE_STR)+": "+aRootCauseJSONObj.getString(Constants.REASON_STR));
						}
					}
					return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
							entity(new GenericEntity<Error>(errors){}).
							header(Constants.X_QUERY_HEADER, selectStatement).
							header(Constants.X_API_ID_HEADER,apiId).
							build();
				}
			}
			CountPost servicePassthru=new CountPost();
			return servicePassthru.handlePassthruService(httpReq, httpResp,null, apiId,null,respStr,null,null,null);
		} 
		catch(UnsupportedEncodingException e){
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.IO_EXEPTION_ERROR_STR+" slect statement="+selectStatement)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}
		catch (JSONException e) {
			//e.printStackTrace();
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.JSON_PARSE_ERROR+" response String="+respStr)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}
		catch (IOException e) {
			//e.printStackTrace();
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.IO_EXEPTION_ERROR_STR+" url="+url+" slect statement="+selectStatement)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}
	}

	public static Response handleSQLGetIds(HttpServletRequest httpReq, 
			HttpServletResponse httpResp, 
			String clause,
			String apiId,
			String scroll,
			String scrollid,
			Integer size){
		WhereClause whereClause=new WhereClause();
		whereClause.setClause(clause);
		return SearchServiceUtil.handleSQLPostIds(httpReq, 
				httpResp, 
				whereClause,
				apiId,
				scroll,
				scrollid,
				size);

	}

	public static Response handleSQLPostIds(HttpServletRequest httpReq, 
			HttpServletResponse httpResp, 
			WhereClause clause,
			String apiId,
			String scroll,
			String scrollid,
			Integer size){

		String respStr="";
		String selectStatement=Constants.SELECT_STAR_STR+SearchInitializer.getSearchIndex()+" WHERE ";
		String url=SearchInitializer.getElastSearchBaseUrl();
		if(!url.endsWith("/")){
			url+="/";
		}
		url+=Constants.EXPLAIN_URL_SUFFIX;
		try {
			if(scrollid==null){
				String theClause=clause.getClause();

				selectStatement+=URLDecoder.decode(theClause, Constants.UTF_STR);
				respStr=SearchServiceUtil.callElasticSearchSqlPlugin(url, selectStatement);
				if(null!=selectStatement && !selectStatement.trim().isEmpty()){
					//remove all extra white space
					selectStatement=selectStatement.replaceAll("\\s+", " ");
					//limit length to X_QUERY_MAX_LENGTH for putting in the header
					if(selectStatement.length()>Constants.X_QUERY_MAX_LENGTH){
						selectStatement=selectStatement.substring(0,Constants.X_QUERY_MAX_LENGTH);
					}
				}
				JSONObject respJSON=new JSONObject(respStr);

				if(respJSON.has(Constants.ERROR_STR)){
					Error errors=new Error();
					JSONObject errorsJSON=respJSON.getJSONObject(Constants.ERROR_STR);
					if(errorsJSON.has(Constants.ROOT_CLAUSE_STR)){
						JSONArray rootCauseJSONArr=errorsJSON.getJSONArray(Constants.ROOT_CLAUSE_STR);

						for(int i=0;i<rootCauseJSONArr.length();++i){
							JSONObject aRootCauseJSONObj=rootCauseJSONArr.getJSONObject(i);
							errors.addError(aRootCauseJSONObj.getString(Constants.TYPE_STR)+": "+aRootCauseJSONObj.getString(Constants.REASON_STR));
						}
					}
					return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
							entity(new GenericEntity<Error>(errors){}).
							header(Constants.X_QUERY_HEADER, selectStatement).
							header(Constants.X_API_ID_HEADER,apiId).
							build();
				}
			}
			RetrievePost servicePassthru=new RetrievePost();
			return servicePassthru.handlePassthruService(httpReq, httpResp, null,apiId,respStr,respStr,scroll,scrollid,size);
		} 
		catch(UnsupportedEncodingException e){
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.IO_EXEPTION_ERROR_STR+" slect statement="+selectStatement)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}
		catch (JSONException e) {
			//e.printStackTrace();
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.JSON_PARSE_ERROR+" response String="+respStr)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}
		catch (IOException e) {
			//e.printStackTrace();
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).
					entity(new GenericEntity<String>((Constants.IO_EXEPTION_ERROR_STR+" url="+url+" slect statement="+selectStatement)){}).
					header(Constants.X_QUERY_HEADER, selectStatement).
					header(Constants.X_API_ID_HEADER,apiId).
					build();
		}

	}
}
