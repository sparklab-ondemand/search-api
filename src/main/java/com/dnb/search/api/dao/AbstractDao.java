package com.dnb.search.api.dao;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.HttpHeaderKeyValuePair;
import com.dnb.search.api.entity.ReturnBody;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;


public abstract class AbstractDao{

	private static final Logger log = Logger.getLogger(AbstractDao.class);
	Map<String,AbstractDao>registeredDao=new HashMap<String,AbstractDao>();

	HttpServletRequest                   httpReq;
	HttpServletResponse                  httpResp;
	long                                 starttime;
	long                                 took;
	Error                                errMessages;
	String                               params;
	String                               query;
	String                               apiid;
	String								 scroll;
	String 								 scrollid;
	Integer 						     size;


	public abstract AbstractDao createProduct(HttpServletRequest httpReq, 
                                              HttpServletResponse httpResp, 
                                              String apiId, 
                                              String params,
                                              String query,
                                              String scroll,
                                              String scrollid, 
                                              Integer size);
	
	public abstract ReturnBody processSearch();
	
	public HttpServletRequest getHttpReq() {
		return httpReq;
	}

	public void setHttpReq(HttpServletRequest httpReq) {
		this.httpReq = httpReq;
	}

	public HttpServletResponse getHttpResp() {
		return httpResp;
	}

	public void setHttpResp(HttpServletResponse httpResp) {
		this.httpResp = httpResp;
	}

	public long getStarttime() {
		return starttime;
	}

	public void setStarttime(long starttime) {
		this.starttime = starttime;
	}

	public long getTook() {
		return took;
	}

	public void setTook(long took) {
		this.took = took;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public static Logger getLog() {
		return log;
	}

	public void setErrMessages(Error errorMessages){
		this.errMessages=errorMessages;
	}

	public Error getErrMessages(){
		return this.errMessages;
	}

	public void setStartTime(){
		this.starttime=System.currentTimeMillis();
	}

	public String getApiid() {
		return apiid;
	}

	public void setApiid(String apiid) {
		this.apiid = apiid;
	}

	public Response makeGetCall(String uri, List<HttpHeaderKeyValuePair> headersKeyValuePairs)throws IOException{
		//A get call sends a null query
		return this.makeRestCall(uri, headersKeyValuePairs, null);
	}

	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}

	public String getScrollid() {
		return scrollid;
	}

	public void setScrollid(String scrollid) {
		this.scrollid = scrollid;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Response makePostCall(String uri, String query, List<HttpHeaderKeyValuePair> headersKeyValuePairs)throws IOException{
		return makeRestCall(uri,headersKeyValuePairs,query);
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	//When queryBody is null it is a GET call, if there is a value, it is a POST call
	private Response makeRestCall(String uri, List<HttpHeaderKeyValuePair>headersKeyValuePairs, String queryBody)throws IOException{
		Builder builder=new Request.Builder();
		builder
		.header("ACCEPT", "application/json")
		.url(uri);
		//.header("Authorization", authorization)
		
		//
		if(null!=queryBody && !queryBody.trim().isEmpty()){
			RequestBody body=RequestBody.create(com.squareup.okhttp.MediaType.parse("application/json"),queryBody);
			builder
			.post(body);
		}
		
		//Now add all the headers if there are any
		if(null!=headersKeyValuePairs){
			for(HttpHeaderKeyValuePair aKeyValuePair:headersKeyValuePairs){
				//Make sure that aKeyValuePair isn't null
				if(null!=aKeyValuePair){
					String aKey=aKeyValuePair.getKey();
					String aValue=aKeyValuePair.getValue();
					//make sure the key and value are not null nor empty
					if(null!=aKey && !aKey.trim().isEmpty() && null!=aValue && !aValue.trim().isEmpty()){
						builder.header(aKeyValuePair.getKey(),aKeyValuePair.getValue());
					}
				}
			}
		}

		Request request=builder.build();
		OkHttpClient httpClient=new OkHttpClient();	
		Response response=httpClient.newCall(request).execute();

		return response;

	}
}
