package com.dnb.search.api.dao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DaoFactory {
	private Map<String, AbstractDao> registeredDaoImpls;
	private static DaoFactory instance;
	
	public void registerDao(String daoId, AbstractDao daoClass){
		this.registeredDaoImpls.put(daoId, daoClass);
	}
	
	public AbstractDao createProduct(String daoId,
			                         HttpServletRequest httpReq, 
			                         HttpServletResponse httpResp, 
			                         String apiId, 
			                         String params,
			                         String query,
			                         String scroll,
			                         String scrollid, 
			                         Integer size){
		AbstractDao dao=this.registeredDaoImpls.get(daoId);
		return dao.createProduct(httpReq, httpResp, apiId, params, query, scroll, scrollid, size);
	}
	
	public boolean isDaoRegistered(String className){
		boolean retVal=false;
		if(null!=DaoFactory.instance && null!=DaoFactory.instance.registeredDaoImpls){
			return DaoFactory.instance.registeredDaoImpls.containsKey(className);
		}
		return retVal;
	}
	
	public static DaoFactory instance(){
		if(null==DaoFactory.instance){
			synchronized(DaoFactory.class){
				if(null==DaoFactory.instance){
					instance=new DaoFactory();
					instance.registeredDaoImpls=new HashMap<String, AbstractDao>();
				}
			}
		}
		return DaoFactory.instance;
	}
}
