package com.dnb.search.api.dao;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.ReturnBody;
import com.squareup.okhttp.Response;
import com.dnb.search.api.entity.Error;

public class CountDaoGetImpl extends AbstractDao{

	
	public AbstractDao createProduct(HttpServletRequest httpReq, 
                                     HttpServletResponse httpResp, 
                                     String apiId, 
                                     String params,
                                     String query,
                                     String scroll,
                                     String scrollid, 
                                     Integer size){
		
		AbstractDao retVal=new CountDaoGetImpl();
		retVal.setHttpReq(httpReq);
		retVal.setHttpResp(httpResp);
		retVal.setApiid(apiId);
		retVal.setParams(params);
		retVal.setQuery(query);
		retVal.setErrMessages(new Error());
				
		
		return retVal;
	}

	public ReturnBody processSearch(){
		ReturnBody retVal=new ReturnBody();
		if(null==query){			
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.SEARCH_PARAMS_REQUIRED+" params="+query);
			retVal.addErrorMessage(Constants.SEARCH_PARAMS_REQUIRED+" params="+query);
			retVal.setReturnMessage(returnMessageJSONObj);			
		}
		else{
			//			JSONObject paramsJSONObject=new JSONObject(query);
			if(null!=super.apiid && !super.apiid.isEmpty() ){ 

				//				if(paramsJSONObject.has(Constants.QUERY_STR)){

				//					JSONObject queryJSON=paramsJSONObject.getJSONObject(Constants.QUERY_STR);												

				String queryStr=query;


				super.setStartTime();

				//Make sure the apiid exists
				if(SearchInitializer.doesApiIdExist(super.apiid)){
					//Now we have to build the REST client

					//Get the url to the elastic search server for the count elasticsearch endpont
					String countUrl=(SearchInitializer.getElasticSearchUrl()+Constants.SEARCH_URL_COMMAND);
					//countUrl should be something like: http://localhost:9200/es_sample_2/_count/
					try {

						//								JSONObject jsonQuery=new JSONObject(queryStr);
						//
						//								JSONObject theQuery=new JSONObject();
						//
						//								theQuery.put(Constants.QUERY_STR, jsonQuery);
						//								//Make this a count command
						//								theQuery.put(Constants.SIZE_STR, 0);
						//								retVal.setQueryRan(queryStr);

						String theQueryStr= countUrl + "?q=" +  queryStr + Constants.SIZE_ZERO;

						//						String authorization=httpReq.getHeader("Authorization");
						//						if(null==authorization){
						//							authorization="";
						//						}
						//								RequestBody body=RequestBody.create(com.squareup.okhttp.MediaType.parse("application/json"),theQueryStr);


						Response response=this.makeGetCall(theQueryStr, null);

						String retStr=response.body().string();
						/**
						 * retStr should be something like: 
			   {
			     "count": 7999305,
			     "_shards": {
			       "total": 5,
			       "successful": 5,
			       "failed": 0
			     }
			   }
						 *  
						 */				
						retVal.setApiid(super.apiid);
						retVal.setQueryRan(queryStr);
						retVal.setResults(retStr);
						retVal.setApiid(super.apiid);
						retVal.setUrl(Constants.ADVANCED_GET_ID_WITH_SCROLL + queryStr);
						retVal.setStatusCode(HttpServletResponse.SC_OK);

						return retVal;
					} 
					catch(IOException e){
						e.printStackTrace();
						retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
						JSONObject returnMessageJSONObj=new JSONObject();
						returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with url:"+countUrl);
						retVal.setReturnMessage(returnMessageJSONObj);
						retVal.addErrorMessage("IOException with url:"+countUrl);
					}
				}
				else{
					retVal.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,super.apiid+" "+Constants.SEARCH_API_DOES_NOT_ACCCESS);
					retVal.setReturnMessage(returnMessageJSONObj);
					retVal.addErrorMessage(Constants.SEARCH_API_DOES_NOT_ACCCESS);
				}
			}
			else{
				retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
				JSONObject returnMessageJSONObj=new JSONObject();
				returnMessageJSONObj.put(Constants.ERROR_STR,super.apiid+" "+Constants.API_HEADER_REQUIRED);
				retVal.setReturnMessage(returnMessageJSONObj);
				retVal.addErrorMessage(Constants.API_HEADER_REQUIRED);
			}
		}
		return retVal;
	}
}
