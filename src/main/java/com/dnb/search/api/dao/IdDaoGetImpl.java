package com.dnb.search.api.dao;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.ReturnBody;
import com.squareup.okhttp.Response;

public class IdDaoGetImpl extends AbstractDao{

//	static{
//		DaoFactory.instance().registerDao(IdDaoGetImpl.class.getName(), new CountDaoPostImpl());
//	}

	public AbstractDao createProduct(HttpServletRequest httpReq, 
			                         HttpServletResponse httpResp, 
			                         String apiId, 
			                         String params,
			                         String query,
			                         String scroll,
			                         String scrollid, 
			                         Integer size){
		
		AbstractDao retVal=new IdDaoGetImpl();
		retVal.setHttpReq(httpReq);
		retVal.setHttpResp(httpResp);
		retVal.setApiid(apiId);
		retVal.setParams(params);
		retVal.setQuery(query);
		retVal.setScroll(scroll);
		retVal.setScrollid(scrollid);
		retVal.setSize(size);

		retVal.setErrMessages(new Error());

		return retVal;
	}

	public ReturnBody processSearch(){

		ReturnBody retVal=new ReturnBody();

		//if both the scrollid and params is empty then we have an error
		if((null==scrollid||scrollid.isEmpty()) && (null==this.params||this.params.isEmpty())){
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.RETRIEVE_IDS_MUST_HAVE_QUERY_OR_SCROLL_ID);
			retVal.addErrorMessage(Constants.RETRIEVE_IDS_MUST_HAVE_QUERY_OR_SCROLL_ID);
			retVal.setReturnMessage(returnMessageJSONObj);	
		}
		//We also have to make sure that there is a X-APP-ID header value
		else if(null==super.apiid || super.apiid.isEmpty()){
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.API_HEADER_REQUIRED);
			retVal.addErrorMessage(Constants.API_HEADER_REQUIRED);
			retVal.setReturnMessage(returnMessageJSONObj);			
		}
		//We have a srollid, we just need to send it to elastic search
		//This code need to be changed to fit a GET request, where the scroll ID is passed in as a Query Param
		else{
			String scrollScanUrl=null;
			//We have a scrollid we can ignore the params
			if(null!=scrollid && !scrollid.isEmpty()){


				retVal.setQueryRan(this.params);
				String url="";
				//Get the url to the elastic search server for the count elasticsearch endpont
				scrollScanUrl=(SearchInitializer.getElastSearchBaseUrl());
				if(!scrollScanUrl.endsWith("/")){
					scrollScanUrl+="/";
				}
				scrollScanUrl+=(Constants.SCROLL_SCAN_SEARCH_URL_SUFFIX+"?"+Constants.SCROLL_ID_STR+"="+scrollid);
				url+=(Constants.SCROLL_SCAN_SEARCH_URL_SUFFIX+"?"+Constants.SCROLL_ID_STR+"="+scrollid);

				//if there is no scroll then set it to 1m
				if(null==scroll || scroll.isEmpty()){
					scroll=Constants.DEFAULT_SCROLL_VALUE;
				}
				scrollScanUrl+=("&"+Constants.SCROLL_STR+"="+this.scroll);
				url+=("&"+Constants.SCROLL_STR+"="+this.scroll);
				retVal.setUrl(url);
				try{

					Response response=this.makeGetCall(scrollScanUrl, null);

					String retStr=response.body().string();
					retVal.setApiid(super.apiid);
					retVal.setResults(retStr);
					retVal.setStatusCode(HttpServletResponse.SC_OK);
				}
				catch(IOException e){
					e.printStackTrace();
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with GET URI:"+scrollScanUrl);
					retVal.setReturnMessage(returnMessageJSONObj);
					retVal.addErrorMessage("IOException with GET URI:"+scrollScanUrl);
				}
			}
			//There is no scrollid, there must be params
			else{
				try{
					////////////////////////////////
					String getIdsUrl=(SearchInitializer.getElasticSearchUrl()+Constants.SEARCH_URL_COMMAND+"?"+Constants.SCAN_SEARCH_TYPE_SUFFIX);

					//build the scroll parameter
					getIdsUrl+=("&"+Constants.SCROLL_STR+"=");
					//Scroll exists and it's not empty
					if(null!=this.scroll&&!this.scroll.trim().isEmpty()){
						getIdsUrl+=this.scroll;								
					}
					else{
						getIdsUrl+=Constants.DEFAULT_SCROLL_VALUE;	
					}
					getIdsUrl+=("&"+Constants.SIZE_STR+"=");
					Integer theSize=Constants.DEFAULT_PAGE_SIZE;
					if(null!=this.size && this.size!=0){
						theSize=this.size;
					}
					getIdsUrl+=theSize;
					String encodedQuery=URLEncoder.encode(this.params,Constants.UTF_STR);
					//build the simple query, if we are in this region of code there must be a q parameter, just get it
					getIdsUrl+=("&"+Constants.QUERY_STR_PARAM+"="+encodedQuery);
					getIdsUrl+="&fields="+Constants.DUNS_STR;
					Response response=this.makeGetCall(getIdsUrl, null);
					String retStr=response.body().string();
					retVal.setSize(theSize);
					retVal.setApiid(super.apiid);
					retVal.setQueryRan(getIdsUrl);
					retVal.setResults(retStr);
					retVal.setApiid(super.apiid);
					retVal.setScroll(this.scroll);
					retVal.setUrl(getIdsUrl);
					retVal.setStatusCode(HttpServletResponse.SC_OK);

					return retVal;				

				}
				catch(UnsupportedEncodingException e){
					//e.printStackTrace();
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,"UnsupportedEncodingException with params:"+this.params);
					retVal.setReturnMessage(returnMessageJSONObj);	
					retVal.addErrorMessage("UnsupportedEncodingException with params:"+this.params);
				}
				catch(IOException e){
					//e.printStackTrace();
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with url:"+scrollScanUrl);
					retVal.setReturnMessage(returnMessageJSONObj);
					retVal.addErrorMessage("IOException with url:"+scrollScanUrl);
				}
			}
		}
		return retVal;
	}
}

