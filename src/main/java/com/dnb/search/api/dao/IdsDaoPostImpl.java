package com.dnb.search.api.dao;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.ReturnBody;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class IdsDaoPostImpl extends AbstractDao{

	private String scroll;
	private String scrollid;
	private int    size;
	
	static{
		DaoFactory.instance().registerDao(IdsDaoPostImpl.class.getName(), new IdsDaoPostImpl());
	}

	public AbstractDao createProduct(HttpServletRequest httpReq, 
                                     HttpServletResponse httpResp, 
                                     String apiId, 
                                     String params,
                                     String query,
                                     String scroll,
                                     String scrollid, 
                                     Integer size){

		AbstractDao retVal=new IdsDaoPostImpl();
		retVal.setHttpReq(httpReq);
		retVal.setHttpResp(httpResp);
		retVal.setApiid(apiId);
		retVal.setQuery(query);
		retVal.setScroll(scroll);
		retVal.setScrollid(scrollid);
		retVal.setSize(size);
		retVal.setQuery(query);
		retVal.setErrMessages(new Error());

		return retVal;
	}

	public ReturnBody processSearch(){
		ReturnBody retVal=new ReturnBody();

		//if both the scrollid and query is empty then we have an error
		if((null==scrollid||scrollid.isEmpty()) && (null==this.query||this.query.isEmpty())){
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.RETRIEVE_IDS_MUST_HAVE_QUERY_OR_SCROLL_ID);
			retVal.addErrorMessage(Constants.RETRIEVE_IDS_MUST_HAVE_QUERY_OR_SCROLL_ID);
			retVal.setReturnMessage(returnMessageJSONObj);	
		}
		//We also have to make sure that there is a X-APP-ID header value
		else if(null==super.apiid || super.apiid.isEmpty()){
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.API_HEADER_REQUIRED);
			retVal.addErrorMessage(Constants.API_HEADER_REQUIRED);
			retVal.setReturnMessage(returnMessageJSONObj);			
		}
		//We have a srollid, we just need to send it to elastic search
		else{
			String queryStr=null;
			//We have a scrollid we can ignore the query
			if(null!=scrollid && !scrollid.isEmpty()){

				//if there is no scroll then set it to 1m
				if(null==scroll || scroll.isEmpty()){
					scroll=Constants.DEFAULT_SCROLL_VALUE;
				}

				retVal.setQueryRan("");
				//Get the url to the elastic search server for the count elasticsearch endpont
				String scrollScanUrl=(SearchInitializer.getElastSearchBaseUrl());
				if(!scrollScanUrl.endsWith("/")){
					scrollScanUrl+="/";
				}
				//The scorllScanUrl should look something like: http://localhost:9200/_search/scroll
				scrollScanUrl+=Constants.SCROLL_SCAN_SEARCH_URL_SUFFIX;

				JSONObject queryJSONObject=new JSONObject();
				try{
					queryJSONObject.put(Constants.SCROLL_STR, scroll);
					queryJSONObject.put(Constants.SCROLL_ID_STR, scrollid);
					queryStr=queryJSONObject.toString();

					Response response=this.makePostCall(scrollScanUrl, queryStr, null);

					String retStr=response.body().string();
					retVal.setApiid(super.apiid);
					retVal.setResults(retStr);
					retVal.setStatusCode(HttpServletResponse.SC_OK);
				}
				catch(JSONException e){
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,Constants.JSON_PARSE_ERROR+" scroll="+scroll+" scrollid="+scrollid);
					retVal.setReturnMessage(returnMessageJSONObj);	
				}
				catch(IOException e){
					e.printStackTrace();
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with queryStr:"+queryStr);
					retVal.setReturnMessage(returnMessageJSONObj);
				}
			}
			//There is no scrollid, there must be a query
			else{
				JSONObject jsonQuery=null;
				String scrollScanUrl=null;
				try{
					if(this.size<=0){
						this.size=Constants.DEFAULT_PAGE_SIZE;
					}
					if(null==this.scroll || this.scroll.isEmpty()){
						this.scroll=Constants.DEFAULT_SCROLL_VALUE;
					}
					scrollScanUrl=(SearchInitializer.getElasticSearchUrl());
					if(!scrollScanUrl.endsWith("/")){
						scrollScanUrl+="/";
					}
					scrollScanUrl+=(Constants.SEARCH_URL_COMMAND+"?"+Constants.SCAN_SEARCH_TYPE_SUFFIX+"&"+
							Constants.SCROLL_STR+"="+this.scroll);
					jsonQuery=new JSONObject(query);
					ArrayList<String> fieldsList = new ArrayList<String>();
					fieldsList.add(Constants.DUNS_CAP_STR);
					jsonQuery.put("fields", fieldsList);
					jsonQuery.put(Constants.SIZE_STR, this.size);
					retVal.setQueryRan(query);

					String theQueryStr=jsonQuery.toString();

					String authorization=httpReq.getHeader("Authorization");
					if(null==authorization){
						authorization="";
					}
					RequestBody body=RequestBody.create(com.squareup.okhttp.MediaType.parse("application/json"),theQueryStr);

					Request request=new Request.Builder()
					.url(scrollScanUrl)
					.header("ACCEPT", "application/json")
					//.header("CONTENT-TYPE", "application/json")
					//.header("Authorization", authorization)
					.post(body)
					.build();
					OkHttpClient httpClient=new OkHttpClient();
					Response response=httpClient.newCall(request).execute();

					String retStr=response.body().string();
					retVal.setApiid(super.apiid);
					retVal.setResults(retStr);
					retVal.setStatusCode(HttpServletResponse.SC_OK);

					return retVal;
				}
				catch(IOException e){
					e.printStackTrace();
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with url:"+scrollScanUrl);
					retVal.setReturnMessage(returnMessageJSONObj);
					retVal.addErrorMessage(Constants.ERROR_STR+": IOException with url:"+scrollScanUrl);
				}
				catch(JSONException e){
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,Constants.JSON_PARSE_ERROR+" query="+query);
					retVal.setReturnMessage(returnMessageJSONObj);	
					retVal.addErrorMessage(Constants.JSON_PARSE_ERROR+": query="+query);
				}
			}
		}
		return retVal;
	}

	public String getScroll() {
		return scroll;
	}

	public void setScroll(String scroll) {
		this.scroll = scroll;
	}

	public String getScrollid() {
		return scrollid;
	}

	public void setScrollid(String scrollid) {
		this.scrollid = scrollid;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
