package com.dnb.search.api.dao;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.dnb.search.api.Constants;
import com.dnb.search.api.SearchInitializer;
import com.dnb.search.api.entity.Error;
import com.dnb.search.api.entity.ReturnBody;
import com.squareup.okhttp.Response;

public class CountDaoPostImpl extends AbstractDao{

//	static{
//		DaoFactory.instance().registerDao(CountDaoPostImpl.class.getName(), new CountDaoPostImpl());
//	}
	
	public AbstractDao createProduct(HttpServletRequest httpReq, 
                                     HttpServletResponse httpResp, 
                                     String apiId, 
                                     String params,
                                     String query,
                                     String scroll,
                                     String scrollid, 
                                     Integer size){
		
		
		AbstractDao retVal=new CountDaoPostImpl();
		retVal.setHttpReq(httpReq);
		retVal.setHttpResp(httpResp);
		retVal.setApiid(apiId);
		retVal.setParams(params);
		retVal.setQuery(query);
		retVal.setErrMessages(new Error());
		
		return retVal;
	}

	public ReturnBody processSearch(){
		ReturnBody retVal=new ReturnBody();
		if(null==this.query){			
			retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
			JSONObject returnMessageJSONObj=new JSONObject();
			returnMessageJSONObj.put(Constants.ERROR_STR,Constants.SEARCH_PARAMS_REQUIRED+" query="+query);
			retVal.addErrorMessage(Constants.SEARCH_PARAMS_REQUIRED+" query="+query);
			retVal.setReturnMessage(returnMessageJSONObj);			
		}
		else{
			try{
				JSONObject paramsJSONObject=new JSONObject(query);
				if(null!=super.apiid && !super.apiid.isEmpty() ){
					
					if(paramsJSONObject.has(Constants.QUERY_STR)){

						JSONObject queryJSON=paramsJSONObject.getJSONObject(Constants.QUERY_STR);												
						
						String queryStr=queryJSON.toString();

						//Make sure the apiid exists
						if(SearchInitializer.doesApiIdExist(super.apiid)){
							//Now we have to build the REST client

							String server=SearchInitializer.getElasticSearchUrl();
							if(!server.endsWith("/")){
								server+="/";
							}
							//Get the url to the elastic search server for the count elasticsearch endpont
							String countUrl=(server+Constants.SEARCH_URL_COMMAND);
							//countUrl should be something like: http://localhost:9200/es_sample_2/_count/
							try {															
								JSONObject jsonQuery=new JSONObject(queryStr);
								JSONObject theQuery=new JSONObject();

								theQuery.put(Constants.QUERY_STR, jsonQuery);
								//Make this a count command
								theQuery.put(Constants.SIZE_STR, 0);
								retVal.setQueryRan(queryStr);
								retVal.setJsonQuery(new JSONObject().put(Constants.QUERY_STR,jsonQuery));

								String theQueryStr=theQuery.toString();

								Response response=this.makePostCall(countUrl, theQueryStr, null);
								String retStr=response.body().string();
								/**
								 * retStr should be something like: 
				                   {
                                     "took": 2,
                                     "time_out": false, 
                                      "_shards": {
                                       "total": 5,
                                       "successful": 5,
                                       "failed": 0
                                     },
                                     "hits": {
                                       "total": 7602921,
                                       "max_score": 0.0,
                                       "hits": []
                                     }
                                   }
								 *  
								 */				
								retVal.setApiid(super.apiid);
								retVal.setResults(retStr);
								retVal.setStatusCode(HttpServletResponse.SC_OK);

								return retVal;
							} 
							catch(IOException e){
								e.printStackTrace();
								retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
								JSONObject returnMessageJSONObj=new JSONObject();
								returnMessageJSONObj.put(Constants.ERROR_STR,"IOException with url:"+countUrl);
								retVal.setReturnMessage(returnMessageJSONObj);
							}
						}
						else{
							retVal.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
							JSONObject returnMessageJSONObj=new JSONObject();
							returnMessageJSONObj.put(Constants.ERROR_STR,super.apiid+" "+Constants.SEARCH_API_DOES_NOT_ACCCESS);
							retVal.setReturnMessage(returnMessageJSONObj);
						}

					}
					else{
						retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
						JSONObject returnMessageJSONObj=new JSONObject();
						returnMessageJSONObj.put(Constants.ERROR_STR,super.apiid+" "+Constants.QUERY_REQUIRED+" query="+query);
						retVal.setReturnMessage(returnMessageJSONObj);
					}

				}
				else{
					retVal.setStatusCode(HttpServletResponse.SC_BAD_REQUEST);
					JSONObject returnMessageJSONObj=new JSONObject();
					returnMessageJSONObj.put(Constants.ERROR_STR,super.apiid+" "+Constants.API_HEADER_REQUIRED);
					retVal.setReturnMessage(returnMessageJSONObj);
				}
			}
			catch(JSONException e){
				retVal.setStatusCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				JSONObject returnMessageJSONObj=new JSONObject();
				returnMessageJSONObj.put(Constants.ERROR_STR,Constants.JSON_PARSE_ERROR+" for params="+query);				
				retVal.setReturnMessage(returnMessageJSONObj);
			}
		}
		return retVal;
	}
}
